# OxyAPI_Project
OxyApp API
###Deployment Platform
- Linux
- Python 2.7.6 or above
- RabbitMQ server

###Dependency
- Django framework
- Django REST framework
- Django REST Swagger
