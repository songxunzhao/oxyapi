#!bin/bash
#debian server configuration
wget http://repo.mysql.com/mysql-apt-config_0.2.1-1debian7_all.deb
dpkg -i mysql-apt-config_0.2.1-1debian7_all.deb
apt-get update
apt-get install mysql-server
wget https://www.rabbitmq.com/rabbitmq-signing-key-public.asc
apt-key add rabbitmq-signing-key-public.asc
apt-get install rabbitmq-server
wget --no-check-certificate https://pypi.python.org/packages/source/s/setuptools/setuptools-1.4.2.tar.gz
tar -xvf setuptools-1.4.2.tar.gz
cd setuptools-1.4.2
python2.7 setup.py install
apt-get install git
pip install virtualenv
virtualenv grid
cd grid
source bin/activate
apt-get install libmysqlclient-dev
apt-get install libpython2.7
apt-get install python-dev
apt-get install libffi-dev
pip install -r packages
apt-get install postgresql postgresql-client
apt-get install iptables-persistent
git clone https://songxunzhao:deltae=deltamc2@github.com/songxunzhao/OxyAPI_Project.git
apt-get install libapache2-mod-wsgi
