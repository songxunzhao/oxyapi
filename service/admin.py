from django.contrib import admin
from grid.models import *
# Register your models here.

@admin.register(WxActivity)
class WxActivityAdmin(admin.ModelAdmin):
	pass

@admin.register(WxCompany)
class WxCompanyAdmin(admin.ModelAdmin):
	pass

@admin.register(WxCompanyMarket)
class WxCompanyMarketAdmin(admin.ModelAdmin):
	pass

@admin.register(WxCompanyMember)
class WxCompanyMemberAdmin(admin.ModelAdmin):
	pass

@admin.register(WxCompanyService)
class WxCompanyServiceAdmin(admin.ModelAdmin):
	pass

@admin.register(WxContract)
class WxContractAdmin(admin.ModelAdmin):
	pass

@admin.register(WxContractStatusType)
class WxContractStatusTypeAdmin(admin.ModelAdmin):
	pass

@admin.register(WxJobPost)
class WxJobPostAdmin(admin.ModelAdmin):
	pass

@admin.register(WxJobPostSkill)
class WxJobPostSkillAdmin(admin.ModelAdmin):
	pass

@admin.register(WxLang)
class WxLangAdmin(admin.ModelAdmin):
	pass

@admin.register(WxLicenceType)
class WxLicenceTypeAdmin(admin.ModelAdmin):
	pass

@admin.register(WxMarketType)
class WxMarketTypeAdmin(admin.ModelAdmin):
	pass

@admin.register(WxUser)
class WxUserAdmin(admin.ModelAdmin):
	pass

@admin.register(WxUserLang)
class WxUserLangAdmin(admin.ModelAdmin):
	pass

@admin.register(WxUserLicence)
class WxUserLicenceAdmin(admin.ModelAdmin):
	pass

@admin.register(WxUserSkill)
class WxUserSkillAdmin(admin.ModelAdmin):
	pass
