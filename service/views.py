import datetime,time,calendar
from itertools import chain
from operator import attrgetter
import os, json, OpenSSL, copy
from apnsclient import *
import hashlib, random, pytz, uuid, urllib

from django.conf import settings as setting
from django.utils import timezone
from django.core.paginator import *
from django.core.exceptions import *
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from django.db.models import *

from .tasks import *
from .mail import *
from . import utils as utils
from .serializers import *
from .file_manager import *
from .authentication import ExpiringTokenAuthentication
from grid.models import *
import grid.settings as settings

from rest_framework import parsers, renderers, generics, views, viewsets
from rest_framework.response import Response
from rest_framework.decorators import (
    api_view,
    parser_classes,
    renderer_classes,
    authentication_classes,
    permission_classes,
    list_route
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000

# STATUS Code definitions
REQUEST_SUCCESS = 'REQUEST_SUCCESS'
INVALID_REQUEST = 'INVALID_REQUEST'
ACCOUNT_EXIST = 'ACCOUNT_EXIST'


@api_view(['GET'])
@parser_classes([parsers.FormParser])
@renderer_classes([renderers.TemplateHTMLRenderer])
def verify_signup(request, format=None, **kwargs):
    """
    Verify signup code. <br>
    This API is called automatically by clicking link on email. Don't call this manually.
    ---
    parameters:
        - name: email
          description: email for user's account
          required: true
          type: string
          paramType: path
        - name: verify_code
          description: verify code emailed at signup
          type: string
          paramType: path
          required: true
    """

    email = kwargs['email']
    verify_code = kwargs['verify_code']
    error_msg = ""

    try:
        user = WxUser.objects.get(email = email)
        utc_now = datetime.datetime.now(pytz.UTC)

        if user.verify_date < utc_now - datetime.timedelta(hours=24):
            error_msg = "Verify code has been expired!"
        elif  user.verify_code != verify_code:
            error_msg = "Verify code is wrong!"
        else:
            error_msg = ""
            user.account_status = 1
            user.save()
    except ObjectDoesNotExist:
        error_msg = "Your email does not exist"

    return Response({'msg' : error_msg}, template_name='verify_result.html')


@api_view(['POST'])
@parser_classes([parsers.FormParser])
@renderer_classes([renderers.JSONRenderer])
def request_recover(request, format=None):
    """
    Request recovery of user's password <br>
    <strong>Request Format: </strong> application/x-www-form-urlencoded
    ---

    type:
        msg:
         required: true
         type: string
    parameters:
        - name: email
          description: email for user's account
          required: true
          type: string
          paramType: form
    """

    email = request.data.get('email', None)

    user_obj = get_object_or_404(get_user_model(), email = email)

    #Generate verify code here
    rand_num = random.randint(10000, 999999)

    if user_obj.is_active():
        user_obj.verify_code = str(rand_num)
        user_obj.verify_date = datetime.datetime.utcnow()
        user_obj.save()

        #ToDo : send email to address
        mail_str = render_to_string(
            'recover_email.html',
            {
                'username': user_obj.name,
                'verify_code': str(rand_num)
            }
        )
        sendmail(settings.DEFAULT_FROM_EMAIL,
                 email,
                 "Reset Your Grid Account Password",
                 mail_str
                 )

    else:
        return Response({'detail' : 'Not found'}, status = 404)

    return Response({'msg' : 'Verify code was sent to your email account, please verify'})


@api_view(['POST'])
@parser_classes([parsers.FormParser])
@renderer_classes([renderers.JSONRenderer])
def recover_password(request, format=None):
    """
    Recover user's password <br>
    <strong>Request Format: </strong> application/x-www-form-urlencoded
    ---
    type:
        msg:
         required: true
         type: string
    parameters:
        - name: email
          description: email for user's account
          required: true
          type: string
          paramType: form
        - name: verify_code
          description: <code>verify_code</code> received from email
          required: true
          type: string
          paramType: form
        - name: pwd
          description: New password
          required: true
          type: string
    """
    email = request.data.get('email', None)
    verify_code = request.data.get('verify_code', None)
    new_pass = request.data.get('pwd', None)

    user_obj = get_object_or_404(get_user_model(), email = email)

    if user_obj.is_active() and user_obj.verify_code == verify_code:
        user_obj.set_password(new_pass)
        user_obj.save()
        return Response({'msg' : 'Successfully changed password'})

    return Response({'detail' : "Not found"}, status = 404)


@api_view(['POST'])
@parser_classes([parsers.JSONParser])
@renderer_classes([renderers.JSONRenderer])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def change_password(request):
    """
    Change user's password <br>
    <strong>Request Format: </strong> application/json
    ---
    type:
        msg:
         required: true
         type: string
        status:
         required: true
         type: number
    parameters:
        - name: body
          description: request body for change password
          required: true
          paramtype: body
          pytype: ChangeUserPasswordSerializer
    """

    validator = ChangeUserPasswordSerializer(data=request.data)

    if validator.is_valid():
        data = validator.validated_data
        user = request.user

        if user.check_password(data.get('old_password')):

            user.set_password(data.get('new_password'))
            user.save()
            return Response({'msg': 'Successfully changed password'})

        else:

            return Response({'msg': 'Password doesn\'t match'}, status=400)

    return Response(
        {'msg': 'Request is invalid', 'detail': validator.errors},
        status = 400
    )


@api_view(['POST'])
@parser_classes([parsers.FormParser])
@renderer_classes([renderers.JSONRenderer])
def verify_request(request, format=None):
    """
    Check verify code <br>
    <strong>Request Format: </strong> application/x-www-form-urlencoded
    ---
    type:
        msg:
         required: true
         type: string
        status:
         required: true
         type: number
    parameters:
        - name: email
          description: email for user's account
          required: true
          type: string
          paramType: form
        - name: verify_code
          description: <code>verify_code</code> received from email
          required: true
          type: string
          paramType: form
    """

    email = request.data.get('email', None)
    verify_code = request.data.get('verify_code', None)

    user_obj = get_object_or_404(get_user_model(), email = email)

    if user_obj.is_active() and user_obj.verify_code == verify_code:
        return Response({'msg' : 'Verification was successful'})

    return Response({'msg' : 'Verification was failed'}, status = 404)


@api_view(['POST'])
@parser_classes([parsers.FormParser])
@renderer_classes([renderers.JSONRenderer])
@authentication_classes([])
def auth(request, format = None):
    """
    Authenticate user. <br> <strong>Request format is application/x-www-form-urlencoded</strong>
    ---

    type:
      token:
        required: true
        type: string

    serializer: AuthSerializer
    omit_serializer: false

    parameters:
        - name: email
          description: email for user's account
          required: true
          type: string
          paramType: form
        - name: password
          description: password for login
          type: string
          paramType: form
          required: true
        - name: device_token
          description: Device token for Apple Push
          type: string
          required: false
          paramType: form
        - name: device_type
          description: Device type
          type: number
          required: false

    responseMessages:
        - code: 400
          message: Not authenticated
        - code: 404
          message: Not found
    """

    device_token = request.data.get("device_token", None)
    device_type = request.data.get("device_type", DeviceType.IOS_DEVICE)

    serializer = AuthSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    user = serializer.validated_data
    if user.is_active():
        token, created = Token.objects.get_or_create(user=user)
        if not(created):
            token.created = datetime.datetime.utcnow()
            token.save()
        content = {
            'user' : {
                'id' : user.id
            },
            'token' : unicode(token.key)
        }

        if device_token:
            #Register device tokens
            device_token = device_token.encode('ascii', errors = 'backslashreplace')
            WxUserDevice.registerDeviceToken(user, device_token, device_type)

    elif user.account_status == 0:    #Account was not verified yet
        content = {
            'msg' : 'This account was not verified, please check your email and verify.'
        }
        return Response(content, status=400)
    return Response(content, status=200)


@api_view(['POST'])
@parser_classes([parsers.MultiPartParser])
@renderer_classes([renderers.JSONRenderer])
@authentication_classes([])
def signup(request, format = None):
    """
    Signup user. Status code is returned by code parameter. <br><br>
    <strong>Status code: </strong> <br>
    REQUEST_SUCCESS: Request was successful <br>
    INVALID_REQUEST: Request is not valid <br>
    ACCOUNT_EXIST: Email is already used by other account <br>
    <strong>Request format:</strong> <br>
    <code>multipart/form-data</code>
    ---
    type:
     msg:
         required: true
         type: string
     code:
         required: true
         type: string

    parameters:
     - name: email
       type: string
       required: true
     - name: name
       type: string
       required: false
     - name: password
       type: string
       required: true
     - name: image
       type: string
       required: false
       description: Base64 encode image

    omit_serializer: false

    responseMessages:
        - code: 400
          message: Request is not valid
    """
    # Re-compose request data.
    request_data = {}
    request_data['email'] = request.data.get('email', None)
    request_data['name'] = request.data.get('name', "")
    request_data['password'] = request.data.get('password', None)

    try:
        user = WxUser.objects.get(email=request_data['email'])
        if user.is_active():
            response = {
                'msg' : 'This email was already used by other account',
                'code' : ACCOUNT_EXIST
            }
            return Response(response, status=400)
        else:
            serializer = ProfileSerializer(user, data = request_data)
            if serializer.is_valid():
                serializer.save()
            else:
                response = {
                    'msg' : 'Request is not valid',
                    'code' : INVALID_REQUEST,
                    'errors' : serializer.errors
                }
                return Response(response, status=400)
    except Exception as exp:
        serializer = ProfileSerializer(data = request_data)
        if serializer.is_valid():
            user = serializer.save()
        else:
            response = {
                'msg' : 'Request is not valid',
                'code' : INVALID_REQUEST,
                'exp' : str(exp),
                'errors' : serializer.errors
            }
            return Response(response, status=400)

    # Delete already uploaded file
    try:
        if user.image_link and not utils.is_url(user.image_link):
            os.remove(setting.UPLOAD_PATH + user.image_link)
    except Exception as ex:
        pass

    #ToDo : Save image for profile.
    filename = handle_base64_file(request.data.get('image', None), '.jpg')
    user.image_link = filename

    #Generate verification code
    rand_num = random.randint(1, 1000)
    m = hashlib.md5()
    m.update(str(rand_num))
    verify_code = m.hexdigest()
    user.verify_code = verify_code
    user.verify_date = timezone.now()
    user.save()

    #ToDo : Send verification email to user here.
    verify_link = request.build_absolute_uri(
        reverse('verify_signup',
                kwargs={"email" : user.email, "verify_code" : verify_code})
    )

    mail_str = render_to_string('signup_verify_email.html', {'link' : verify_link})
    bsent_mail = sendmail(
        settings.DEFAULT_FROM_EMAIL,
        user.email,
        "Grid Account Confirmation",
        mail_str
    )

    if bsent_mail:
        response = {
                'msg' : 'Email was sent to your account, verify your email',
                'code' : REQUEST_SUCCESS
            }
        return Response(response)
    else:
        response = {
            'msg' : 'Problem occured while trying to send email, please try again later'
        }
        return Response(response, status = 400)


@api_view(['POST'])
@parser_classes([parsers.MultiPartParser])
@renderer_classes([renderers.JSONRenderer])
@authentication_classes([])
def socialAuth(request, format = None):
    """
    Sign up social user. This API supports for Facebook, Twitter and LinkedIn.
    Social network type must be specified in parameters.
    Profile image can be specified by image file or image link. If image link is specified, image file is ignored.
    social_type parameter can be 0 for Facebook, 1 for Twitter, 2 for LinkedIn <br>
    <strong> Request format is multipart/form-data</strong>
    ---
    type:
        token:
            required: true
            type: string

    parameters:
     - name: social_id
       type: number
       required: true
     - name: social_type
       type: number
       required: true
     - name: image
       type: string
       required: false
       description: Base64 encoded image
     - name: device_token
       type: string
       required: false
       description: Device token for push notification
     - name: device_type
       type: number
       required: false
       description: Device type

    omit_serializer: false
    responseMessages:
        - code: 400
          message: Not authenticated
    """
    
    social_id = request.data.get('social_id', False)
    social_type =  int(request.data.get('social_type', False))
    img_data = request.data.get('image', False)
    device_type = request.data.get('device_type', DeviceType.IOS_DEVICE)
    device_token = request.data.get('device_token', None)
    email = request.data.get('email', '')

    if social_type == 0:
        social_str = "facebook"
    elif social_type == 1:
        social_str = "twitter"
    else:
        social_str = "linkedIn"

    user = None
    try:
        user = WxUser.objects.get(email=email)
    except Exception:
        #recompose dict from request data
        data = {}
        if social_id:
            data[u'email'] = social_str + social_id
        data[u'social_type'] = social_type
        data[u'social_id'] = social_id

        #Save image data to file
        filename = handle_base64_file(img_data, '.jpg')
        data[u'image_link'] = filename

        serializer = ProfileSerializer(data = data)
        if serializer.is_valid():
            user = serializer.save()

    if user != None:    
        token, created = Token.objects.get_or_create(user=user)
        if not(created):
            token.created = datetime.datetime.utcnow()
            token.save()

        content = {
            'token' : unicode(token.key)
        }

        if device_token:
            #Register device tokens
            device_token = device_token.encode('ascii', errors = 'backslashreplace')
            WxUserDevice.registerDeviceToken(user, device_token, device_type)

        return Response(content)
    else:
        content = {
            'msg' : 'Authentication failed'
        }
        return Response(content, status = 400)


class UserDetail(generics.RetrieveAPIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ProfileSerializer
    queryset = WxUser.objects.all()

    def get(self, request, **kwargs):        
        """
        Retrieve user's information. User's information defers from profile in where this API provides information for other users than current user.
        ---
        response_serializer: ProfileSerializer
        """
        return super(UserDetail, self).get(self, request, **kwargs)


class UserList(generics.ListAPIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated, )
    serializer_class = ProfileSerializer
    queryset = WxUser.objects.all()
    def get_queryset(self):
        keyword = self.request.QUERY_PARAMS.get("keyword", None)
        word_list = keyword.split(' ')
        keyword_joined = ''
        for word in word_list:
            keyword_joined = keyword_joined + word + '*' + ' '

        return WxUser.objects.filter(name__search = keyword_joined)

    def get(self, request, **kwargs):
        """
        Search users with certain keyword.
        ---
        response_serializer: ProfileSerializer
        parameters:
         - name: keyword
           paramType: query
           required: true
        """
        keyword = request.QUERY_PARAMS.get("keyword", None)
        if keyword is None:
            return Response({
                    "msg" : "Missing required parameters"
                }, status = 400)

        return super(UserList, self).get(self, request, **kwargs)


class Profile(views.APIView):

    authentication_classes = (ExpiringTokenAuthentication,)
    parser_classes = (
        parsers.JSONParser,
    )
    renderer_classes = (
        renderers.JSONRenderer,
    )
    permission_classes = (IsAuthenticated,)
    def get(self, request, format = None):
        """
        Retrieve profile. <br> <strong>Request format is application/json</strong>
        ---
        response_serializer: ProfileSerializer
        """
        user = request.user
        serializer = ProfileSerializer(user)

        model = serializer.data
        return Response(model)
    def put(self, request, format = None):
        """
        Update profile. End point must update data with response after calling this API!<br>
        <strong>Request format is application/json</strong>
        ---
        omit_parameters:
            - form
        parameters:
         - name: content
           paramType: body
           pytype: ProfileSerializer
        """

        user = request.user
        data = request.data

        img_data = data.get("image_link", None)
        if img_data != None:
            # Delete saved images
            try:
                if user.image_link and not utils.is_url(user.image_link):
                    os.remove(setting.UPLOAD_PATH + user.image_link)
            except Exception:
                pass

            filename = handle_base64_file(img_data, '.jpg')
            data.update({'image_link':filename})

        serializer = ProfileSerializer(user, data=data,  partial=True)

        if serializer.is_valid():
            serializer.save()
            instance = serializer.data

            return Response(instance)
        return Response(serializer.errors, status = 400)


class ProfileLanguage(views.APIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    renderer_classes = (
        renderers.JSONRenderer,
    )
    permission_classes = (IsAuthenticated,)

    def get(self, request, format = None):
        """
        Retrieve language list of user
        """
        user = request.user
        serializer = UserLangSerializer(user.lang, many = True)
        return Response(serializer.data)
    def post(self, request, format = None):
        """
        Add language to user's profile
        ---
        serializer: UserLangSerializer
        """
        request.data['user'] = request.user
        serializer = UserLangSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['user'] = request.user
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status = 400)

    def delete(self, request, format = None):
        """
        Delete language to user's profile
        ---
        parameters:
         - name: user_lang_id
           type: number
           paramType: query
        """
        update_id = request.QUERY_PARAMS.get('user_lang_id', False)
        if update_id:
            try:
                instance = WxUserLang.objects.get(pk=update_id)
                if instance.user == request.user:
                    instance.delete()
                    return Response({
                        'msg' : 'Successfully removed'
                        })
                else:
                    return Response({
                        'msg' : "Permission denied"
                        }, status = 403)
            except Exception as ex:
                return Response({
                        'msg' : ex
                    }, status = 403)            
        else:
            return Response({
                'msg' : 'Missing required parameters'
                }, status = 400)


class ProfileSkill(views.APIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    renderer_classes = (
        renderers.JSONRenderer,
    )
    permission_classes = (IsAuthenticated,)

    def get(self, request, format = None):
        """
        Retrieve skill list from user's profile
        """
        user = request.user
        serializer = UserSkillSerializer(user.skill, many = True)
        return Response(serializer.data)

    def post(self, request, format = None):
        """
        Add skill to user's profile
        ---
        serializer: UserSkillSerializer
        """
        request.data['user'] = request.user
        serializer = UserSkillSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['user'] = request.user
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status = 400)
    def put(self, request, format = None):
        """
        Update skill of user's profile
        ---
        parameters:
         - name: user_skill_id
           type: number
        serializer: UserSkillSerializer
        """

        update_id = request.data.get('user_skill_id', False)
        if update_id:
            instance = WxUserSkill.objects.get(pk=update_id)
        else:
            return Response({
                'msg' : 'Missing required parameters'
                }, status = 400)

        serializer = UserSkillSerializer(instance, data=request.data, partial=True)
        if serializer.is_valid():

            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status = 400)
            
    def delete(self, request, format = None):
        """
        Delete skill from user's profile
        ---
        parameters:
         - name: user_skill_id
           type: number
           paramType: query
        """

        update_id = request.QUERY_PARAMS.get('user_skill_id', False)
        if update_id:
            try:
                instance = WxUserSkill.objects.get(pk=update_id)
                if instance.user == request.user:
                    instance.delete()
                    return Response({
                        'msg' : 'Successfully removed'
                        })
                else:
                    return Response({
                        'msg' : "Permission denied"
                        }, status = 403)
            except Exception as ex:
                return Response({
                        'msg' : ex
                    }, status = 403)
        else:
            return Response({
                'msg' : 'Missing required parameters'
                }, status = 400)

class ProfileLicence(views.APIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    renderer_classes = (
        renderers.JSONRenderer,
    )
    permission_classes = (IsAuthenticated,)
    def get(self, request, format = None):
        """
        Retrieve licence list from user's profile.
        """
        serializer = UserLicenceSerializer(request.user.licence, many = True)
        return Response(serializer.data)

    def post(self, request, format = None):
        """
        Add licence to user's profile
        ---
        serializer: UserLicenceSerializer
        """
        serializer = UserLicenceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['user'] = request.user
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status = 400)

    def delete(self, request, format = None):
        """
        Delete licence from user's profile
        ---
        parameters:
         - name: user_licence_id
           type: number
           paramType: query
        """

        update_id = request.QUERY_PARAMS.get('user_licence_id', False)
        if update_id:
            try:
                instance = WxUserLicence.objects.get(pk=update_id)
                if instance.user == request.user:
                    instance.delete()
                    return Response({
                        'msg' : 'Successfully removed'
                        })
                else:
                    return Response({
                        'msg' : "Permission denied"
                        }, status = 403)
            except Exception as ex:
                return Response({
                        'msg' : ex
                    }, status = 403)
        else:
            return Response({
                'msg' : 'Missing required parameters'
                }, status = 400)

class ProfileTimeline(views.APIView):
    authentication_classes = (ExpiringTokenAuthentication, )
    renderer_classes = (
        renderers.JSONRenderer,
        )
    permission_classes = (IsAuthenticated, )
    def get(self, request, format = None):
        """
        Retrieve timeline list of user.
        ---
        response_serializer : UserTimelineSerializer
        """
        user = request.user
        serializer = UserTimelineSerializer(user.timeline, many = True)
        return Response(serializer.data)

    def post(self, request, format = None):
        """
        Add one record of working history to user's timeline list.
        ---
        serializer : UserTimelineSerializer 
        """
        user = request.user
        serializer = UserTimelineSerializer(data = request.data)
        if serializer.is_valid():
            serializer.validated_data['user_id'] = user.id
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status = 400)

    def delete(self, request, format = None):
        """
        Delete one record of working history from user's timeline list.
        ---
        parameters:
         - name: pk
           type: number
           paramType: query
        """
        user = request.user
        remove_id = request.QUERY_PARAMS.get('pk', False)
        if remove_id:
            try:
                obj = WxUserTimeline.objects.get(pk=remove_id)
                if obj.user != user:
                    return Response(
                        {
                            'msg' : 'Access denied for object'
                        },
                        status = 403
                        )

                obj.delete()
                return Response({
                        'msg' : 'Successfully removed'
                    })
            except Exception as e:
                return Response(
                            {
                            'msg' : 'Object does not exist'
                            }, status = 404
                        )
        return Response(
                        {
                            'msg' : 'Missing required parameters'
                        },
                        status = 400
                    )

    def put(self, request, format = None):
        """
        Change one record of working history
        ---
        parameters:
         - name: id
           type: number
        serializer : UserTimelineSerializer
        """
        user = request.user
        pk = request.data.get("id")
        try:
            obj = user.timeline.get(pk = pk)
            serializer = UserTimelineSerializer(obj, data = request.data, partial = True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors)
        except Exception as ex:
            return Response({
                    'msg' : ex
                },
                status = 404
                )

# --- Skill search part ---
class Skill(viewsets.GenericViewSet):
    renderer_classes = (renderers.JSONRenderer,)
    pagination_class = StandardResultsSetPagination
    serializer_class = SkillSerializer

    @list_route(methods = ['post'], )
    def search(self, request):
        """
        Search skills by keyword.<br/>
        Request body type is application/x-www-form-urlencoded.
        <code>page</code> and <code>page_size</code> parameters are query parameters,
        <code>keyword</code> parameter is form parameter.

        ---
        response_serializer: SkillSerializer
        parameters_strategy: replace
        parameters:
         - name: keyword
           paramType: string
           required: true
         - name: page
           paramType: query
           required: false
         - name: page_size
           paramType: query
           required: false

        omit_serializer: false

        responseMessages:
            - code: 400
              keyword: keyword field must be specified
        """
        
        keyword = request.data.get('keyword', None)
        if keyword is None:
            return Response(
                {'keyword' : 'keyword field must be specified'},
                status = 400
            )

        queryset = WxSkills.objects.filter(Q(name__istartswith=keyword),
                                           Q(group_id__isnull=True))

        paged_queryset = self.paginate_queryset(queryset)
        serializer = SkillSerializer(paged_queryset, many=True)
        return self.get_paginated_response(serializer.data)

    @list_route()
    def grouplist(self ,request):
        """
        Get all skill categories
        ---
        parameters_strategy: replace
        parameters:
         - name: page
           paramType: query
         - name: page_size
           paramType: query

        response_serializer: SkillSerializer

        omit_serializer: false

        """
        queryset = WxSkills.objects.filter(Q(group_id__isnull = True))

        paged_queryset = self.paginate_queryset(queryset)
        serializer = SkillSerializer(paged_queryset, many=True)
        return self.get_paginated_response(serializer.data)

# --- Education search part --- 
class Education(viewsets.GenericViewSet):
    renderer_classes = (renderers.JSONRenderer,)
    pagination_class = StandardResultsSetPagination
    serializer_class = EduSerializer
    
    @list_route(methods = ['post'], )
    def search(self, request):
        """
        Search education by keyword.<br/>
        Request body type is application/x-www-form-urlencoded.
        <code>page</code> and <code>page_size</code> parameters are query parameters,
        <code>keyword</code> parameter is form parameter.

        ---
        response_serializer: EduSerializer
        parameters_strategy: replace
        parameters:
         - name: keyword
           paramType: string
           required: true
         - name: page
           paramType: query
           required: true
         - name: page_size
           paramType: query
           required: true

        omit_serializer: false

        responseMessages:
            - code: 400
              keyword: keyword field must be specified
        """
        
        keyword = request.data.get('keyword', None)
        if keyword is None:
            return Response(
                {'keyword' : 'keyword field must be specified'},
                status = 400
            )

        queryset = WxEducation.objects.filter(Q(name__istartswith=keyword),
                                              Q(group_id__isnull=True))

        paged_queryset = self.paginate_queryset(queryset)
        serializer = EduSerializer(paged_queryset, many=True)
        return self.get_paginated_response(serializer.data)

    @list_route()
    def grouplist(self ,request):
        """
        Get all education categories
        ---
        parameters_strategy: replace
        parameters:
         - name: page
           paramType: query
           required: true
         - name: page_size
           paramType: query
           required: true

        response_serializer: EduSerializer

        omit_serializer: false

        """
        queryset = WxEducation.objects.filter(Q(group_id__isnull = True))

        paged_queryset = self.paginate_queryset(queryset)
        serializer = EduSerializer(paged_queryset, many=True)
        return self.get_paginated_response(serializer.data)

# --- Quality search part ---
class Quality(viewsets.GenericViewSet):
    renderer_classes = (renderers.JSONRenderer,)
    pagination_class = StandardResultsSetPagination    
    serializer_class = QualitySerializer
    @list_route(methods = ['post'], )
    def search(self, request):
        """
        Search quality by keyword.<br/>
        Request body type is application/x-www-form-urlencoded.
        <code>page</code> and <code>page_size</code> parameters are query parameters,
        <code>keyword</code> parameter is form parameter.

        ---
        response_serializer: QualitySerializer
        parameters_strategy: replace
        parameters:
         - name: keyword
           paramType: string
           required: true
         - name: page
           paramType: query
           required: true
         - name: page_size
           paramType: query
           required: true

        omit_serializer: false

        responseMessages:
            - code: 400
              keyword: keyword field must be specified
        """
        
        keyword = request.data.get('keyword', None)
        if keyword is None:
            return Response(
                {'keyword' : 'keyword field must be specified'},
                status = 400
            )

        queryset = WxQuality.objects.filter(Q(name__istartswith=keyword),
                                            Q(group_id__isnull=True))

        paged_queryset = self.paginate_queryset(queryset)
        serializer = QualitySerializer(paged_queryset, many=True)
        return self.get_paginated_response(serializer.data)

    @list_route()
    def grouplist(self ,request):
        """
        Get all quality categories
        ---
        parameters_strategy: replace
        parameters:
         - name: page
           paramType: query
           required: true
         - name: page_size
           paramType: query
           required: true

        response_serializer: QualitySerializer

        omit_serializer: false

        """
        queryset = WxQuality.objects.filter(Q(group_id__isnull = True))
        paged_queryset = self.paginate_queryset(queryset)
        serializer = QualitySerializer(paged_queryset, many=True)

        return self.get_paginated_response(serializer.data)

# Job field search
class JobTitleSampleView(generics.ListAPIView):
    authentication_classes = []
    serializer_class = JobTitleSampleSerializer
    def get_queryset(self):
        queryset = WxJobTitleSample.objects.all()
        return queryset

class JobFieldView(views.APIView):
    authentication_classes = []

    def get(self, request, format = None):
        """
        Get job field list
        ---
        response_serializer: JobFieldSerializer
        parameters:
         - name: field_name
           paramType: query
           type: string
           description: Field name which user typed
        """

        field_name = request.QUERY_PARAMS.get('field_name', None)
        typed_field_list = field_name.split(' ')
        query_name_list = copy.copy(typed_field_list)
        query_val = ''
        for query_name in query_name_list:
            query_val = query_val + '+' + query_name + ' '

        queryset = WxJobField.objects.filter(name__search=query_val)

        #Return all title samples
        # serializer = JobFieldSerializer(queryset, many = True)
        # return Response(serializer.data, status = 200)

        #Return root title samples
        result_list = []
        for record in queryset:
            fields = reversed(record.name.split(' '))
            last_idx = -1
            for field in fields:
                bfound = False
                for idx, typed_field in enumerate(typed_field_list):
                    if(field.lower() == typed_field.lower()):
                        bfound = True
                        last_idx = idx
                        break
                if not bfound:
                    new_field_list = copy.copy(typed_field_list)
                    if last_idx == -1:
                        new_field_list.append(field)
                    else:
                        new_field_list.insert(last_idx, field)

                    result_list.append({
                        "field_name" :field ,
                        "future_title": ' '.join(new_field_list)
                        })
                    break
        return Response(
            {
                'field_list' : result_list
            }, status = 200)

    def post(self, request, format = None):
        """
        Add new job field candidate
        ---
        parameters:
         - name: field_name
           paramType: form
           type: string
           description: Name of job field to be added
        responseMessages:
         - code : 400
           message : Missing required parameters
        """
        field_name = request.data.get('field_name', None)
        field_name = field_name.strip()
        if field_name is None or field_name == '':
            return Response({
                    'msg': 'Missing required parameters'
                }, status = 400)

        # Add job field into database
        serializer = JobFieldSerializer(data = {'name' : field_name})
        if serializer.is_valid():
            serializer.save()
            return Response({
                'msg': 'Successfully added job field'
                }, status = 200)

#-----------------------
# Job Post part
#-----------------------
class JobPostView(views.APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (ExpiringTokenAuthentication, )
    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):
        """
        Post job.<br><strong> This API requires authentication. Request type is application/json </strong>
        ---
        parameters:
         - name: content
           paramType: body
           pytype: JobPostSerializer
           description: Job post content

        """
        content = request.data
        content['user_id'] = request.user.id
        serializer = JobPostSerializer(data = content)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status = 400)

    def put(self, request, format = None):
        """
        Update posted job<br>
        <strong> This API requires authentication. Request type is application/json </strong>
        ---
        parameters:
         - name: content
           paramType: body
           pytype: JobPostSerializer
           description: Job post content
        """
        content = request.data
        instance = WxJobPost.objects.get(id=content['id'])
        if instance.user_id != request.user.id:
            return Response({
                    'msg' : 'You have no right to change this job.'
                }, status = 400)

        serializer = JobPostSerializer(instance, data = content)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status = 400)

class JobSearchView(views.APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (ExpiringTokenAuthentication, )

    def post(self, request, format = None):
        """
        Search published jobs.
        ---
        request_serializer: JobSearchCriteriaSerializer
        """
        req_serializer = JobSearchCriteriaSerializer(data = request.data)
        if req_serializer.is_valid():
            input_data = req_serializer.validated_data

            keyword = input_data.get('keyword', "")
            job_level = input_data.get('job_level', None)
            skill = input_data.get('skill', None)
            location = input_data.get('location', "")

            queryset = WxJobPost.objects.filter(
                title__istartswith=keyword
            ).filter(
                publish_date__isnull = False
            )

            if job_level:
                queryset = queryset.filter(level = job_level)

            if location:
                location.replace(",", "")
                location.replace("+", "")
                queryset = queryset.filter(location__search = location)

            if skill:
                queryset = queryset.filter(skill_set__name__in = skill)

            resp_serializer = JobPostSerializer(instance = queryset, many = True)
            return Response(resp_serializer.data)

        return Response("Input data is not valid", status = 400)

class OpenJobView(views.APIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        """
        Get open jobs.
        This API works different for two roles - employer and freelancer. For freelancer, it shows open jobs he has placed bid. For employer it shows open jobs he has published.
        ---
        parameters:
         - name: role
           type: string
           required: false
           paramType: query
           description: user's role
        response_serializer: JobPostSerializer
        """
        role = request.QUERY_PARAMS.get("role", "f")
        #Freelancer open jobs.
        if role == "f" :
            serializer = JobPostSerializer(self.freelancer_open_jobs(), many = True)
            return Response(serializer.data)
        else:
            #Employer open jobs
            serializer = JobPostSerializer(self.employer_open_jobs(), many = True)
            return Response(serializer.data)
    def freelancer_open_jobs(self):
        # TODO : not implemented yet.
        return []

        
    def employer_open_jobs(self):
        #TODO: This part should be changed after getting hire part done.
        user_id = self.request.user.id
        queryset = WxJobPost.objects.filter(
            user_id = user_id,
            publish_date__isnull = False
        )
        return queryset

class DashboardView(views.APIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self, request, format = None):
        """
        Get user's dashboard information
        ---
        type:
            num_jobs:
                type: number
                required: true
            num_jobs_views:
                type: number
                required: true
            num_applicants:
                type: number
                required: true
            recent_activity:
                type: array
                required: true
            jobs:
                type: array
                required: true
        """
        resultset = {
        }
        user = request.user
        user_id = user.id
        queryset = WxJobPost.objects.filter(
            user_id = user_id,
            publish_date__isnull = False
        )

        num_views = 0
        for job_record in queryset:
            num_views = num_views + job_record.num_view

        num_applicants = 0
        for job_record in queryset:
            num_applicants = num_applicants + job_record.applicant_list.count()

        resultset['num_jobs'] = queryset.count()
        resultset['num_jobs_views'] = num_views
        resultset['num_applicants'] = num_applicants
        
        activity_types = [NotificationType.JOB_APPLIED, 
                        NotificationType.JOB_EXPIRED, 
                        NotificationType.JOB_REJECTED,
                        NotificationType.JOB_ACCEPTED]
        notification_set = WxNotification.objects.filter(type__in = activity_types)

        resultset['recent_activity'] = NotificationSerializer(
            notification_set,
            many=True
        ).data
        resultset['jobs'] = JobPostSerializer(queryset, many = True).data
        return Response(resultset, status = 200)

# Job template view
class JobTemplateView(generics.ListAPIView):
    authentication_classes = (ExpiringTokenAuthentication, )
    serializer_class = JobPostSerializer
    pagination_class = StandardResultsSetPagination
    def get_queryset(self):
        if(self.request.user):
            user_id = self.request.user.id
            queryObj = Q(user_id = user_id) & Q(publish_date__isnull = False)
            queryset = WxJobPost.objects.filter(queryObj)

            return queryset
        else:
            return []
        
    def get(self, request):
        """
        Get job template list that user has created.
        ---
        response_serializer: JobPostSerializer
        """
        queryset = self.get_queryset()
        serializer = JobPostSerializer(queryset, many = True)
        return Response(serializer.data)

class JobDetailView(views.APIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    def get(self, request, **kwargs):
        """
        Get job detail information
        """    
        user = request.user
        pk = kwargs['pk']
        try:
            job = WxJobPost.objects.get(pk = pk)
            serializer = JobPostSerializer(job)
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response(
                    {
                        'msg' : "Job doesn't exist"
                    },
                    status = 404
                )
        
@api_view(['GET'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated,])
@renderer_classes([renderers.JSONRenderer])
def jobApplyView(request, format = None, **kwargs):
    """
    Apply job
    ---
    type:
     msg:
      type: string
      description: Response message
    """
    user = request.user
    job_id = kwargs['pk']
    data = {
        "job_post" : job_id,
        "user" : user.id
    }

    serializer = JobApplicationSerializer(data = data)
    if serializer.is_valid():
        serializer.save()
        return Response({
                'msg' : "Successfully applied for job"
            })
    else:
        return Response(serializer.errors, status = 400)

@api_view(['GET'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated,])
@renderer_classes([renderers.JSONRenderer])
def jobRetractView(request, format = None, **kwargs):
    """
    Retract job application
    ---
    type:
     msg:
      type: string
      description: Response message
    """

    user = request.user
    pk = kwargs['pk']
    try:
        applicant = WxJobApplicant.objects.get(pk = pk)
        if applicant.user_id == user.id:
            applicant.delete()
            return Response({
                    'msg' : "Successfully retracted"
                })
        else:
            return Response({
                    'msg' : "You didn't applied for this job"
                }, status = 404)
    except ObjectDoesNotExist:
        return Response({
                'msg' : "You didn't applied for this job"
            },
            status = 404)


class JobApplicantsView(views.APIView):
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [ExpiringTokenAuthentication]
    serializer_class = UserApplicantSerializer
    def get(self, request, **kwargs):
        """
        Get all applicants for posted job. API caller must be authenticated as job owner.
        ---
        response_serializer: UserApplicantSerializer
        """
        pk = kwargs['pk']
        try:
            job_post = WxJobPost.objects.get(pk = pk)
            if job_post.user_id != request.user.id:
                return Response({
                        'msg' : "You have no right to check this job"
                    }, status = 400)

            users = []
            for applicant in job_post.applicant_list.select_related().all():
                users.append(applicant.user)
            serializer = UserApplicantSerializer(
                users,
                many = True,
                context={'job' : job_post}
            )
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response(
                {
                    'msg' : 'Job does not exist'
                },
                status = 404
                )
#Hobbies
class HobbySearchView(generics.GenericAPIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = HobbySerializer
    pagination_class = StandardResultsSetPagination

    def post(self, request, **kwargs):
        """
        Search hobby list. Pagination is available.
        ---
        type:
          id:
            required: true
            type: number
          name:
            required: true
            type: number
        parameters_strategy: replace
        parameters:
          - name: page
            type: number
            paramType: query
          - name: page_size
            type: number
            paramType: query
          - name: exclude
            type: string
            description: json list of hobbies to be excluded          
            paramType: form
        """
        keyword = kwargs['keyword']
        exclude_str = request.data.get('exclude', "[]")
        try:
            exclude_list = json.JSONDecoder().decode(exclude_str)
        except Exception:
            exclude_list = []

        queryset = WxHobby.objects.filter(
            name__istartswith=keyword
        ).exclude(
            name__in=exclude_list
        )
        page = self.paginate_queryset(queryset)
        serializer = HobbySerializer(page, many=True)
        return self.get_paginated_response(serializer.data)

class HobbyListView(generics.ListAPIView):
    """
    Get list of all hobbies
    """
    
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = HobbySerializer
    queryset = WxHobby.objects.all()
    def get_queryset(self):
        return WxHobby.objects.all()

    def list(self, request):
        queryset = self.get_queryset()
        serializer = HobbySerializer(queryset, many = True)
        return Response(serializer.data)
#Lang
class LangViewSet(viewsets.ViewSet):
    authentication_classes = (ExpiringTokenAuthentication,)
    renderer_classes = (renderers.JSONRenderer,)
    pagination_class = None

    def list(self, request):
        """
        Get all languages registered in Grid Waxara.
        ---
        type:
            lang_id:
                required: true
                type: number
            name:
                required: true
                type: string
            iso_code:
                required: true
                type: string
        omit_parameters:
            - query
        """
        queryset = WxLang.objects.all()
        serializer = LangSerializer(queryset, many = True)
        return Response(serializer.data)

#Notification List views
class NotificationListView(generics.ListAPIView):
    queryset = WxNotification.objects.all()
    serializer_class = NotificationSerializer
    pagination_class = StandardResultsSetPagination

    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        """
        Get notification list. Authentication is necessary for this API.
        ---
        response_serializer: NotificationSerializer
        parameters:
         - name: page
           paramType: query
           required: true
         - name: page_size
           paramType: query
           required: true
        responseMessages:
         - code: 400
           message: Not authenticated
        """

        user = request.user
        queryset = WxNotification.objects.filter(receipt_id = user.id)
        page = self.paginate_queryset(queryset)
        serializer = NotificationSerializer(page, many = True)
        return self.get_paginated_response(serializer.data)

class SalesNotificationView(generics.ListAPIView):
    serializer_class = NotificationSerializer
    pagination_class = StandardResultsSetPagination

    authentication_classes = (ExpiringTokenAuthentication, )
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        """
        Get notification list for sales. Authentication is required for this API.
        ---
        response_serializer: NotificationSerializer
        parameters:
         - name: page
           paramType: query
           required: false
         - name: page_type
           paramType: query
           required: false
        responseMessages:
         - code: 400
           message: Not Authenticated
        """

        user = request.user
        queryset = WxNotification.objects.filter(
            receipt_id = user.id,
            type__gte = NotificationType.DEAL_ADDED,
            type__lte = NotificationType.DEAL_EXPIRED
        )

        page = self.paginate_queryset(queryset)
        serializer = NotificationSerializer(page, many = True)

        return self.get_paginated_response(serializer.data)

#Job description template view
class JobDescTemplateView(viewsets.ViewSet):
    """
    Operate job description templates.
    """
    authentication_classes = (ExpiringTokenAuthentication, )
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        queryObj = Q(created_user__isnull=True)
        if(self.request.user):
            user_id = self.request.user.id
            queryObj = queryObj | Q(created_user = user_id)

        queryset = WxJobDescTemplate.objects.filter(queryObj)
        return queryset

    def list(self, request):
        """
        Get job description template list. Response is array of objects.
        ---
        response_serializer: JobDescTemplateSimpleSerializer
        """

        queryset = self.get_queryset()
        serializer = JobDescTemplateSerializer(queryset, many = True)
        return Response(serializer.data)
    def retrieve(self, request, pk=None):
        """
        Retrieve job description template.
        ---
        response_serializer: JobDescTemplateSerializer
        """

        obj = get_object_or_404(self.get_queryset(), pk = pk)
        serializer = JobDescTemplateSerializer(obj)
        return Response(serializer.data)


##################
# Friend related part
##################
class FriendListView(generics.ListAPIView):
    """
    Get friend list of user. Authentication is necessary for this API.
    ---
    """
    authentication_classes = (ExpiringTokenAuthentication, )
    permission_classes = (IsAuthenticated,)
    serializer_class = FriendSerializer
    def get_queryset(self):
        keyword = self.kwargs.get('keyword', None)
        user = self.request.user

        #Two queries can do same, but which one is faster - to be tested, current SQL execution load is same for each queries.
        queryset = WxUser.objects.filter(
            (Q(firstFriends__type = 0) & Q(firstFriends__seconduser = user.id))
            | (Q(secondFriends__type = 0) & Q(secondFriends__firstuser = user.id))
        )

        if keyword:
            queryset = queryset.filter(name__istartswith = keyword)
        queryset = queryset.order_by('name')

        # queryset = WxFriendship.objects.raw('''
        #         SELECT wx_user.id id, wx_user.name name, wx_user.image_link image_link
        #         from wx_friendship join wx_user on wx_user.id = wx_friendship.secondUser
        #         where wx_friendship.firstUser = %s
        #         union
        #         SELECT wx_user.id id, wx_user.name name 
        #         from wx_friendship join wx_user on wx_user.id = wx_friendship.firstUser
        #         where wx_friendship.secondUser = %s
        #         order by name
        #         ''' % (user.id, user.id))

        return queryset

#############
#    Friend invitation view
#############

class FriendOperationView(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]
    def post(self, request, **kwargs):
        """
        Invite friend
        ---
        type:
            msg:
                required: true
                type: string
        parameters:
         - name: pk
           type: number
           required: true
           paramType: path
           description: Id of user to invite
        """
        pk = kwargs['pk']
        try:
            user = WxUser.objects.get(pk = pk)
        except ObjectDoesNotExist:
            return Response({
                'msg' : 'User does not exist'
                }, status = 404)

        #User can't invite him as a friend.
        if(request.user == user):
            return Response({
                'msg' : "This user isn't allowed to follow"
            }, status = 400)

        #Check if user is already friend of current user
        if request.user.id > user.id :
            firstuser = user
            seconduser = request.user
        else:
            firstuser = request.user
            seconduser = user
        try:
            WxFriendship.objects.get(firstuser = firstuser, seconduser = seconduser)
            return Response({
                    'msg' : 'This user is already friend'
                }, status = 404)
        except ObjectDoesNotExist:
            pass

        #ToDo: call celery task to send notification to user
        return Response({
                'msg' : 'Invitation was sent to user'
            })

    def delete(self,request, **kwargs):
        """
        Remove friend
        ---
        type:
            msg:
                required: true
                type: string
        parameters:
         - name: pk
           type: number
           required: true
           paramType: path
           description: Remove user's friend
        """
        pk = kwargs['pk']
        user = request.user

        if user.id < pk:
            firstuser = user.id
            seconduser = pk
        else:
            firstuser = pk
            seconduser = user.id

        try:
            friendship = WxFriendship.objects.get(
                firstuser = firstuser,
                seconduser = seconduser
            )
            friendship.delete()
            return Response({
                'msg' : 'Friend was removed'
                })
        except ObjectDoesNotExist:
            return Response({
                    'msg' : 'Friend does not exist'
                }, status = 404)


###
#    Calendar Event Views
###

class EventView(views.APIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        """
        Get events in certain period of time.
        ---
        response_serializer: EventSerializer
        parameters:
         - name: from
           type: number
           required: true
           paramType: query
           description: The time in unix timestamp which the period starts.
         - name: to
           type: number
           required: true
           paramType: query
           description: The time in unix timestamp which the period ends.
        """
        from_day = request.QUERY_PARAMS.get("from", 0)
        to_day = request.QUERY_PARAMS.get("to", 0)
        

    def post(self, request):
        """
        Add event to calendar
        ---
        serializer: EventSerializer
        """
        serializer = EventSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status = 400)
    def delete(self, request):
        """
        Delete event from calendar
        ---
        parameters:
         - name: pk
           type: number
           required: true
           paramType: query
           description: Calendar event id
        """
        pk = request.QUERY_PARAMS.get("pk", None)
        user_id = request.user.id
        if pk:
            calendar_obj = WxCalendar.objects.get(pk = pk)
        else:
            return Response({"msg" : "Missing required parameters"}, status = 400)

###
#    Contact Views
###
class ChatContactView(viewsets.ViewSet):
    authentication_classes= (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated, )

    def list(self, request):
        """
        Get user's contact list
        ---
        """
        serializer = ChatContactSerializer(request.user.chatContacts, many = True)
        return Response(serializer.data)

    def create(self, request):
        """
        Add user's contact
        ---
        parameters:
         - name: contact_id
           type: number
           required: true
           paramType: form
        """

        contact_id = request.data.get('contact_id', None)
        if contact_id is None:
            return Response({
                    "msg" : "Missing required parameters"
                }, status = 400)
        record_info = {
            'user' : request.user.id,
            'contact' : contact_id,
            'status' : 1 # Pending status
        }

        serializer = ChatContactSerializer(data = record_info)
        if serializer.is_valid():
            try:
                serializer.save()
                ###
                #    Send announcement message here. This is delayed process
                ###
                payload = {'to' : str(contact_id), 
                            'message' : "You have received contact request"}
                postChatMessage.delay(payload)

                ###
                #    Send notification
                ###
                opposite = WxUser.objects.get(pk=contact_id)
                n_serializer = NotificationSerializer.createContactNotification(
                    request.user
                )
                postNotification.delay(n_serializer.data, opposite.devices.all())
                
                return Response(serializer.data)
            except Exception as ex:
                return Response({
                        "msg" : str(ex)
                    }, status = 500)

        return Response({
                "msg" : serializer.errors
            }, status = 400)
    @list_route(methods = ['get'])
    def search(self, request):
        """
        Search user's contact list
        ---
        parameters:
         - name: keyword
           type: number
           paramType: query
           required: true
        """

        keyword = request.QUERY_PARAMS.get("keyword", None)
        if keyword is None:
            return Response({"msg" : "Missing required parameters"}, status = 400)
        
        keyword_joined = ''
        word_list = keyword.split(' ')
        for word in word_list:
            keyword_joined = keyword_joined + word + '*' + ' '

        serializer = ChatContactSerializer(
            request.user.chatContacts.filter(contact__name__search = keyword_joined),
            many = True
        )

        return Response(serializer.data)

#Company profile
class CompanyProfile(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CompanySerializer

    def get(self, request):
        """
        Get company profile. Authentication is required for this API.
        ---
        response_serializer: CompanySerializer
        """
        user = request.user
        try:
          objCompany = WxCompany.objects.get(user = user)
          serializer = CompanySerializer(objCompany)
          return Response(serializer.data)
        except ObjectDoesNotExist:
          return Response({'msg' : "Company doesn't exist"}, status = 404)

    def put(aself, request):
        """
        Update company profile. If company profile wasn't created, it is created.
        Authentication is required for this API.
        ---
        serializer: CompanySerializer
        parameters:
         - name: user
           description: User id for company, this field is not required
           required: false
           type: integer
           paramType: form
        """
        user = request.user
        data = request.POST.copy()
        try:
          objCompany = WxCompany.objects.get(user = user)
          serializer = CompanySerializer(objCompany, data = data, partial = True)
          if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
          return Response(data, status = 400)

        except ObjectDoesNotExist:
          data.update({'user' : user.id})
          serializer = CompanySerializer(data = data)
          if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
          return Response(serializer.errors, status = 400)


class CompanyMarket(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """
        Get market list of company. Authentication is required for this API.
        ---
        response_serializer: CompanyMarketSerializer
        """
        user = request.user
        try:
            objCompany = WxCompany.objects.get(user = user)
            serializer = CompanyMarketSerializer(objCompany.market_list, many=True)
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 404)

    def post(self, request):
        """
        Add market to company. Authentication is required for this API.
        ---
        serializer: CompanyMarketSerializer
        """
        user = request.user
        try:
            objCompany = WxCompany.objects.get(user = user)
            data = request.POST.copy()
            data.update({'company' : objCompany.id})
            serializer = CompanyMarketSerializer(data = data)
            serializer.save()
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 400)
    def delete(self, request):
        """
        Delete market from company. Authentication is required for this API.
        ---
        parameters:
         - name: market_id
           description: ID of market
           type: integer
           paramType: query
        """
        user = request.user
        try:
            numMarketID = request.query_params.get('market_id', None)

            objCompany = WxCompany.objects.get(user = user)
            objCompanyMarket = WxCompanyMarket.objects.get(pk = numMarketID)

            if objCompanyMarket.company.id == objCompany.id:
                objCompanyMarket.delete()
            else:
                return Response({'msg' : "You can't delete this market"}, status = 400)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 404)


class CompanyMember(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """
        Get memeber list of company. Authentication is required for this API.
        ---
        response_serializer: CompanyMemberSerializer
        """
        user = request.user
        try:
            objCompany = WxCompany.objects.get(user = user)
            serializer = CompanyMemberSerializer(objCompany.member_list, many=True)
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 400)
    def post(self, request):
        """
        Add member to company. Authentication is required for this API.
        ---
        serializer: CompanyMemberSerializer
        """
        user = request.user
        try:
            objCompany = WxCompany.objects.get(user = user)

            data = request.POST.copy()
            data.update({'company' : objCompany.id})
            serializer = CompanyMemberSerializer(data = data)
            serializer.save()
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 404)

    def delete(self, request):
        """
        Delete member from company. Authentication is required for this API.
        ---
        parameters:
         - name: member_id
           description: ID of member
           type: integer
           paramType: query
        """
        user = request.user
        try:
            numMemberID = request.query_params.get('member_id', None)
            objCompany = WxCompany.objects.get(user = user)
            objCompanyMember = WxCompanyMember.objects.get(pk = numMemberID)

            if objCompanyMember.company.id == objCompany.id:
                objCompanyMember.delete()
            else:
                return Response({'msg' : "You can't delete this member"}, status = 400)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 404)


class CompanyService(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """
        Get service list of company. Authentication is required for this API.
        ---
        response_serializer: CompanyServiceSerializer
        """
        user = request.user
        try:
            objCompany = WxCompany.objects.get(user = user)
            serializer = CompanyServiceSerializer(objCompany.service_list, many=True)
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 404)
    def post(self, request):
        """
        Add service to company. Authentication is required for this API.
        ---
        serializer: CompanyServiceSerializer
        """
        user = request.user
        try:
            objCompany = WxCompany.objects.get(user = user)
            data = request.POST.copy()
            data.update({'company' : objCompany.id})
            serializer = CompanyServiceSerializer(data = data)
            serializer.save()
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 404)

    def delete(self, request):
        """
        Delete service from company. Authentication is required for this API.
        ---
        parameters:
         - name: service_id
           description: ID of service
           type: integer
           paramType: query
        """
        user = request.user
        numServiceID = request.query_params.get('service_id', None)
        try:
            objCompany = WxCompany.objects.get(user = user)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 404)

        try:
            objCompanyService = objCompany.service_list.get(pk=numServiceID)
            if objCompanyService.company.id == objCompany.id:
                objCompanyService.delete()
            else:
                return Response({'msg' : "You can't delete this service"}, status = 400)
        except ObjectDoesNotExist:
            return Response({'msg' : "Service doesn't exist"}, status = 404)


class CompanyBenefit(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """
        Get benefit list of company. Authentication is required for this API.
        ---
        response_serializer: CompanyBenefitSerializer
        """
        user = request.user
        try:
            objCompany = WxCompany.objects.get(user = user)
            serializer = CompanyBenefitSerializer(objCompany.benefit_list, many=True)
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 404)
    def post(self, request):
        """
        Add benefit item to company. Authentication is required for this API.
        ---
        serializer: CompanyBenefitSerializer
        """
        user = request.user
        try:
            objCompany = WxCompany.objects.get(user = user)
            data = request.POST.copy()
            data.update({'company' : objCompany.id})
            serializer = CompanyBenefitSerializer(data = data)
            serializer.save()
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 404)

    def delete(self, request):
        """
        Delete benefit item from company. Authentication is required for this API.
        ---
        parameters:
         - name: benefit_id
           description: ID of benefit
           type: integer
           paramType: query
        """
        user = request.user
        numBenefitID = request.query_params.get('benefit_id', None)
        try:
            objCompany = WxCompany.objects.get(user = user)
        except ObjectDoesNotExist:
            return Response({'msg' : 'You have no company'}, status = 404)

        try:
            objCompanyBenefit = objCompany.benefit_list.get(pk=numBenefitID)
            if objCompanyBenefit.company.id == objCompany.id:
                objCompanyBenefit.delete()
            else:
                return Response({'msg' : "You can't delete this service"}, status = 400)
        except ObjectDoesNotExist:
            return Response({'msg' : "Service doesn't exist"}, status = 404)


class DashboardTaskList(generics.ListAPIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = WxTask.objects.all()
    serializer_class = TaskSerializer
    pagination_class = StandardResultsSetPagination
    def get_queryset(self):
        user = self.request.user
        return user.task_list.order_by('is_checked').all()

    def get(self, request):
        """
        Get task list
        ---
        response_serializer: TaskSerializer
        parameters:
         - name: page
           paramType: query
           required: true
         - name: page_size
           paramType: query
           required: true
        responseMessages:
         - code: 400
           message: Not authenticated
        """
        return super(DashboardTaskList, self).get(self, request)


class DashboardTask(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]
    def get(self, request):
      """
      Do action on task. Supported actions are 'check', 'uncheck'.
      ---
      parameters:
       - name: id
         type: Number
         required: true
         paramType: query
       - name: action
         type: String
         required: true
         paramType: query
         response_serializer: TaskSerializer
      """
      user = request.user
      numTaskID = request.query_params.get('id', None)
      strAction = request.query_params.get('action', None)
      if strAction == 'check':
        try:
          task = user.task_list.get(pk=numTaskID)
          task.is_checked = True
          task.save()
        except ObjectDoesNotExist:
          return Response({'msg' : 'Task was not found'}, status = 404)
      elif strAction == 'uncheck':
        try:
          task = user.task_list.get(pk=numTaskID)
          task.is_checked = False
          task.save()
        except ObjectDoesNotExist:
          return Response({'msg' : 'Task was not found'}, status = 404)
      else:
        return Response({'msg' : strAction}, status = 400)
      return Response({'msg' : 'Successfully processed action'})
    def post(self, request):
      """
      Add task
      ---
      parameters:
       - name: desc
         type: string
         required: true
         paramType: form
      response_serializer: TaskSerializer
      """
      user = request.user
      objRequest = {
        'desc' : request.data.get('desc', ''),
        'is_checked' : False,
        'created_date' : int(time.time()),
        'user' : user.id
      }
      serializer = TaskSerializer(data = objRequest)
      if serializer.is_valid():
        serializer.save()

        #Add notification
        WxNotification.createNotification(NotificationType.TASK_ADDED, 
                                          user.id, 
                                          'You have just added a task', 
                                          serializer.data).save()

        return Response(serializer.data)
      return Response({'msg' : serializer.errors})

    def delete(self, request):
      """
      Delete task
      ---
      parameters:
       - name: task_id
         type: string
         required: true
         paramType: query
      responseMessages:
       - code: 400
         message: You can't delete this task
       - code: 404
         message: Task doesn't exist
      """
      user = request.user
      numTaskID = request.query_params.get('task_id', None)
      try:
          task = WxTask.objects.get(pk = numTaskID)
          if(task.user.id != user.id):
              return Response({'msg' : "You can't delete this task"}, status = 400)
          task.delete()
          return Response({'msg' : 'Successfully removed'})
      except ObjectDoesNotExist:
          return Response({'msg' : "Task doesn't exist"}, status = 404)


@api_view(['POST'])
@parser_classes([parsers.JSONParser])
@renderer_classes([renderers.JSONRenderer])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def create_deal_batch(request, format = None, **kwargs):
  """
  Create multiple deal
  ---
  parameters:
    - name: DealSerializer
      type: WriteDealSerializer
      paramType: body
  """
  serializer = DealSerializer(
      data = request.data,
      many = True,
      context = {'request' : request}
  )

  if serializer.is_valid():
    serializer.save()

    notifications = []
    historys = []

    for deal in serializer.data:
      rate = get_usd_exchange_rate(deal['valueCurrency'])
      if rate < 0:
          rate = 1
      valueInUSD = float(deal['value']) * rate
      valueInUSD = round(valueInUSD,2)
      historys.append({'value':valueInUSD, 'stage':deal['stage'], 'deal':deal['id']})

      #Add notification
      dicDeal = deal
      notifications.append(
          WxNotification.createNotification(
              NotificationType.DEAL_ADDED,
              request.user.id,
              'You have just started new lead',
              dicDeal)
      )
    historySerializer = DealHistorySerializer(data=historys, many = True)

    if historySerializer.is_valid():
        historySerializer.save()
    else:
      return Response(historySerializer.errors)

    WxNotification.objects.bulk_create(notifications)

    return Response(serializer.data)

  return Response(serializer.errors, status = 400)


class DealFilterView(generics.ListAPIView):
    authentication_classes = (ExpiringTokenAuthentication, )
    permission_classes = (IsAuthenticated,)
    serializer_class = DealSerializer

    def get(self, request):
        """
        Get deal list of deal by filters. Authentication is necessary for this API.
        ---

        type:
            token:
                required: true
                type: string

            omit_serializer: true

        parameters:
            - name: stage
              description: Sales stage<br/>Leads = 1000, Contacted, Offer made, In negotiation, Deal Closed
              type: number
              paramType: query
              required: true
        responseMessages:
            - code: 400
              message: Not authenticated
        """
        fstage = request.QUERY_PARAMS.get('stage', False)
        if fstage:
            try:
                user = request.user
                queryset = user.deals.filter(stage=fstage)
                resp_serializer = DealSerializer(instance=queryset, many=True)
                return Response(resp_serializer.data)
                # return Response({
                #     'msg' : 'Successfully removed'
                #     })
            except Exception as ex:
                return Response({
                        'msg' : ex
                    }, status = 403)            
        else:
            return Response({
                'msg' : 'Missing required parameters'
                }, status = 400)        
        # return super(DealFilteriew, self).get(self, request)


class DealListView(generics.ListAPIView):
    """
    Get deal list of deal. Authentication is necessary for this API.
    ---
    """
    authentication_classes = (ExpiringTokenAuthentication, )
    permission_classes = (IsAuthenticated,)
    serializer_class = DealSerializer
    def get_queryset(self):
        user = self.request.user
        return user.deals.all()

class DealView(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request):
        """
        Create new deal.<br/>
        valueCurrency : USD, GBP, RUB, CNY. Following <a href='https://en.wikipedia.org/wiki/ISO_4217'>ISO 4217</a><br/>
        closingProbability : Numeric value between 0 ~ 100<br/>
        stage : Leads = 1000, Contacted, Offer made, In negotiation, Deal Closed<br/>
        closingExpDate : <a href='https://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 Specification</a><br/>
        Reference.<br/>
        <a href='http://stackoverflow.com/questions/17558859/convert-iso-8601-to-nsdate'>Convert ISO 8601 to NSDate</a>
        ---

        type:
          token:
            required: true
            type: string

        omit_serializer: true

        parameters:
            - name: customer
              description: Customer's id
              required: true
              type: number
              paramType: form
            - name: title
              description: Deal title
              type: string
              paramType: form
              required: true
            - name: value
              description: Deal value
              type: number
              paramType: form
              required: true
            - name: valueCurrency
              description: Currency of the deal value which one from USD, GBP, RUB, CNY
              type: string
              paramType: form
              required: true
            - name: stage
              description: Sales stage
              type: number
              paramType: form
              required: true
            - name: closingExpDate
              description: String of date in ISO 8601 format. for example, 2015-09-28T22:17:30Z
              type: string
              paramType: form
              required: true
            - name: closingProbability
              description: Currency of the deal value
              type: number
              paramType: form
              required: true  
            - name: note
              description: Additional note
              type: string
              paramType: form
              required: false  


        responseMessages:
            - code: 400
              message: Not authenticated
            - code: 404
              message: Not found
        """
        user = request.user
        data = request.data
        data['user'] = user.id
        # return Response({'msg' : data, 'data' : data})
        serializer = DealSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            rate = get_usd_exchange_rate(serializer.data['valueCurrency'])
            if rate < 0:
                rate = 1
            valueInUSD = float(serializer.data['value']) * rate
            valueInUSD = round(valueInUSD,2)

            history = {
                'value':valueInUSD,
                'stage':serializer.data['stage'],
                'deal':serializer.data['id']
            }
            historySerializer = DealHistorySerializer(data=history)

            if historySerializer.is_valid():
                historySerializer.save()
            else:
                return Response({'msg' : historySerializer.errors, 'data' : history})

            #Add notification
            dicDeal = serializer.data
            WxNotification.createNotification(
                NotificationType.DEAL_ADDED,
                user.id,
                'You have just started new lead',
                dicDeal
            ).save()

            return Response(serializer.data)
        return Response({'msg' : serializer.errors, 'data' : data})

    def put(self, request):
        """
        Update exist deal.<br/>
        valueCurrency : USD, GBP, RUB, CNY. Following <a href='https://en.wikipedia.org/wiki/ISO_4217'>ISO 4217</a><br/>
        closingProbability : Numeric value between 0 ~ 100<br/>
        stage : Leads = 1000, Contacted, Offer made, In negotiation, Deal Closed<br/>
        closingExpDate : <a href='https://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 Specification</a><br/>
        Reference.<br/>
        <a href='http://stackoverflow.com/questions/17558859/convert-iso-8601-to-nsdate'>Convert ISO 8601 to NSDate</a>
        ---

        type:
          token:
            required: true
            type: string

        omit_serializer: true

        parameters:
            - name: id
              description: Primary key
              required: true
              type: number
              paramType: form
            - name: customer
              description: Customer's id
              required: false
              type: number
              paramType: form
            - name: title
              description: Deal title
              type: string
              paramType: form
              required: false
            - name: value
              description: Deal value
              type: number
              paramType: form
              required: false
            - name: valueCurrency
              description: Currency of the deal value which one from USD, GBP, RUB, CNY
              type: string
              paramType: form
              required: false
            - name: stage
              description: Sales stage
              type: number
              paramType: form
              required: false
            - name: closingExpDate
              description: String of date in ISO 8601 format. for example, 2015-09-28T22:17:30Z
              type: string
              paramType: form
              required: false
            - name: closingProbability
              description: Currency of the deal value
              type: number
              paramType: form
              required: false  
            - name: note
              description: Additional note
              type: string
              paramType: form
              required: false  


        responseMessages:
            - code: 400
              message: Not authenticated
            - code: 404
              message: Not found
        """
        user = request.user

        try:
            objDeal = user.deals.get(pk = request.data['id']) # WxDeal.objects.get(pk = request.data['id'])
            serializer = DealSerializer(objDeal, data = request.data, partial=True)
            if serializer.is_valid():
                serializer.save()

                rate = get_usd_exchange_rate(serializer.data['valueCurrency'])
                if rate < 0:
                    rate = 1
                valueInUSD = float(serializer.data['value']) * rate
                valueInUSD = round(valueInUSD,2)
                history = {
                    'value':valueInUSD,
                    'stage':serializer.data['stage'],
                    'deal':serializer.data['id']
                }
                historySerializer = DealHistorySerializer(data=history)
                if historySerializer.is_valid():
                    historySerializer.save()
                else:
                    return Response({'msg' : historySerializer.errors, 'data' : history})

                #Add notification
                dicDeal = serializer.data
                if dicDeal['stage'] == DealStageType.CLOSED:
                    notifyStr = '%s deal was closed' % (dicDeal['title'])
                    notifyType = NotificationType.DEAL_CLOSED
                else:
                    notifyStr = '{} deal has become {}'.format (
                        dicDeal['title'], DealStageType.toString(dicDeal['stage'])
                    )

                    notifyType = NotificationType.DEAL_STATUS_CHANGED

                WxNotification.createNotification(notifyType,
                                            user.id,
                                            notifyStr,
                                            dicDeal
                                            ).save()
                #Send notification to all members
                teamMember = user.team_member.all()[:1]
                if len(teamMember) > 0:
                    recordSet = WxTeamMember.objects.filter(
                        team_uid = teamMember[0].team_uid
                    )
                    for record in recordSet:
                        if record.user_id == user.id:
                            continue
                        WxNotification.createNotification(notifyType, 
                                                        record.user_id,
                                                        notifyStr,
                                                        dicDeal
                                                        ).save()
                return Response(serializer.data)
            return Response({'msg' : serializer.errors})

        except ObjectDoesNotExist:
            return Response(
                {'msg': 'You have no deal of pk = ' + request.data['id']},
                status = 404
            )
    
    def delete(self, request, format = None):
        """
        Delete exist
        ---
        parameters:
         - name: id
           type: number
           paramType: query
        """
        delete_id = request.QUERY_PARAMS.get('id', False)
        if delete_id:
            try:
                user = request.user
                instance = user.deals.get(pk = delete_id)
                instance.delete()
                return Response({
                    'msg' : 'Successfully removed'
                    })
            except Exception as ex:
                return Response({
                        'msg' : ex
                    }, status = 403)            
        else:
            return Response({
                'msg' : 'Missing required parameters'
                }, status = 400)


class DealInvoiceView(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, **kwargs):
        """
        Get invoice for specified deal.<br/>
        ---
        responseMessages:
            - code: 400
              message: Not authenticated
            - code: 404
              message: Specified deal doesn't exist or has not invoice yet
        """    
        user = request.user
        deal_id = kwargs['pk']
        try:
            objDeal = user.deals.get(pk = deal_id)
            objInvoice = objDeal.invoice
            serializer = DealInvoiceSerializer(objInvoice)
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response(
                    {'msg' : "Specified deal doesn't exist or has not invoice yet"},
                    status = 404
            )

    def post(self, request, **kwargs):
        """
        Create new invoice for specified deal.<br/>

        Reference:<br/>
        <a href='http://stackoverflow.com/questions/17558859/convert-iso-8601-to-nsdate'>Convert ISO 8601 to NSDate</a>
        ---

        type:
          token:
            required: true
            type: string

        omit_serializer: true

        parameters:
            - name: appearedDate
              description: String of date in ISO 8601 format. for example, 2015-11-10T22:17:30Z.
              type: string
              paramType: form
              required: false
            - name: paymentDue
              description: String of date in ISO 8601 format. for example, 2015-11-10T22:17:30Z.
              type: string
              paramType: form
              required: false


        responseMessages:
            - code: 400
              message: Not authenticated
            - code: 404
              message: Not found
        """
        user = request.user
        data = request.data
        deal_id = kwargs['pk']

        try:
            objDeal = user.deals.get(pk = deal_id)
            
            data.update({'deal' : deal_id})
            serializer = DealInvoiceSerializer(data=data)

            if serializer.is_valid():
                serializer.save()

                #Add notification
                notify_string = 'You just sent invoice to %s' % (objDeal.customer.name)
                WxNotification.createNotification(
                    NotificationType.INVOICE_SENT,
                    user.id,
                    notify_string,
                    serializer.data
                ).save()
                
                return Response(serializer.data)
            return Response({'msg' : serializer.errors, 'data' : data})
        except ObjectDoesNotExist:
            return Response({'msg': 'You have no deal of pk = ' + deal_id}, status = 404)

    def put(self, request, **kwargs):
        """
        Create new or update exist invoice for specified deal.<br/>

        Reference:<br/>
        <a href='http://stackoverflow.com/questions/17558859/convert-iso-8601-to-nsdate'>Convert ISO 8601 to NSDate</a>
        ---

        type:
          token:
            required: true
            type: string

        omit_serializer: true

        parameters:
            - name: appearedDate
              description: String of date in ISO 8601 format. for example, 2015-11-10T22:17:30Z.
              type: string
              paramType: form
              required: false
            - name: paymentDue
              description: String of date in ISO 8601 format. for example, 2015-11-10T22:17:30Z.
              type: string
              paramType: form
              required: false


        responseMessages:
            - code: 400
              message: Not authenticated
            - code: 404
              message: Not found
        """
        user = request.user
        data = request.data.copy()
        deal_id = kwargs['pk']

        try:
            objDeal = user.deals.get(pk = deal_id)
        except ObjectDoesNotExist:
            return Response({'msg': 'You have no deal of pk = ' + deal_id}, status = 404)

        try:
            objInvoice = objDeal.invoice
            serializer = DealInvoiceSerializer(objInvoice, data =  data, partial = True)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(data, status = 400)
        except ObjectDoesNotExist:
            data.update({'deal' : deal_id})
            serializer = DealInvoiceSerializer(data = data)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response({'msg' : serializer.errors, 'data' : data})


class DealInvoiceProductView(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]
    def post(self, request, **kwargs):
        """
        Add new product into the invoice of specified deal.<br/>

        Reference:<br/>
        <a href='http://stackoverflow.com/questions/17558859/convert-iso-8601-to-nsdate'>Convert ISO 8601 to NSDate</a>
        ---
        parameters:
            - name: product
              description: Product name
              type: string
              paramType: form
              required: true
            - name: q
              description: Quantity, Ineteger value
              type: string
              paramType: form
              required: true
            - name: netPrice
              description: NetPrice value, Decimal value
              type: number
              paramType: form
              required: true
            - name: tax
              description: Tax value, Integer value
              type: number
              paramType: form
              required: true

        responseMessages:
            - code: 400
              message: Not authenticated
            - code: 404
              message: Not found
        """
        user = request.user
        data = request.data.copy()
        deal_id = kwargs['pk']

        try:
            objDeal = user.deals.get(pk = deal_id)
            objInvoice = objDeal.invoice

            data.update({'invoice' : objInvoice.id})
            serializer = DealInvoiceProductSerializer(data=data)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response({'msg' : serializer.errors, 'data' : data})
        except ObjectDoesNotExist:
            return Response({'msg': 'You have no deal of pk = ' + deal_id}, status = 404)

    def put(self, request, **kwargs):
        """
        Update specified product<br/>

        Reference:<br/>
        <a href='http://stackoverflow.com/questions/17558859/convert-iso-8601-to-nsdate'>Convert ISO 8601 to NSDate</a>
        ---

        type:
          token:
            required: true
            type: string

        omit_serializer: true
        
        parameters:
            - name: id
              description: id
              type: string
              paramType: form
              required: true
            - name: product
              description: Product name
              type: string
              paramType: form
              required: false
            - name: q
              description: Quantity, Ineteger value
              type: string
              paramType: form
              required: false
            - name: netPrice
              description: NetPrice value, Decimal value
              type: number
              paramType: form
              required: false
            - name: tax
              description: Tax value, Integer value
              type: number
              paramType: form
              required: false

        responseMessages:
            - code: 400
              message: Not authenticated
            - code: 404
              message: Not found
        """
        user = request.user
        data = request.data.copy()
        deal_id = kwargs['pk']

        try:
            objDeal = user.deals.get(pk = deal_id)
        except ObjectDoesNotExist:
            return Response({'msg': 'You have no deal of pk = ' + deal_id}, status = 404)

        try:
            objInvoice = objDeal.invoice
            objProduct = objInvoice.products.get(pk = data['id'])
            
            data.update({'invoice' : objInvoice.id})
            serializer = DealInvoiceProductSerializer(
                objProduct,
                data =  data,
                partial = True
            )

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(data, status = 400)
        except ObjectDoesNotExist:
            return Response({'msg': 'No exist product = ' + data['id']}, status = 404)

    def delete(self, request, **kwargs):
        """
        Delete exist all products of Invoice
        ---
        """
        user = request.user
        data = request.data.copy()
        deal_id = kwargs['pk']

        try:
            objDeal = user.deals.get(pk = deal_id)
        except ObjectDoesNotExist:
            return Response({'msg': 'You have no deal of pk = ' + deal_id}, status = 404)

        try:
            objInvoice = objDeal.invoice
            objProducts = objInvoice.products

            for objProduct in objProducts.all():
              objProduct.delete()
            
            return Response({'msg' : 'All products are removed'})
        except ObjectDoesNotExist:
            return Response({'msg' : 'All products are removed'})

class DealInvoiceProductDeleteView(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]
    def delete(self, request, **kwargs):
        """
        Delete specified product of Invoice
        ---
        """
        user = request.user
        data = request.data.copy()
        deal_id = kwargs['pk']
        product_id = kwargs['ppk']

        try:
            objDeal = user.deals.get(pk = deal_id)
        except ObjectDoesNotExist:
            return Response({'msg': 'You have no deal of pk = ' + deal_id}, status = 404)

        try:
            objInvoice = objDeal.invoice
            objProduct = objInvoice.products.get(pk = product_id)
            objProduct.delete()
            
            return Response({'msg' : 'Product is removed'})
        except ObjectDoesNotExist:
            return Response({'msg' : 'No Exist invoice or product'})

@api_view(['POST'])
@parser_classes([parsers.JSONParser])
@renderer_classes([renderers.JSONRenderer])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def create_customer_batch(request, format = None, **kwargs):
  """
  Create multiple customer
  ---
  parameters:
    - name: CustomerSerializer
      type: WriteCustomerSerializer
      paramType: body
  """
  serializer = CustomerSerializer(
      data = request.data,
      many = True,
      context = {'request' : request}
  )

  if serializer.is_valid():
    serializer.save()

    notifications = []
    #Add notification
    for customer in serializer.data:
      notifyStr = '%s customer was added' % (customer['name'])
      notifications.append(
        WxNotification.createNotification(NotificationType.CUSTOMER_ADDED,
                                    request.user.id,
                                    notifyStr,
                                    customer))
    WxNotification.objects.bulk_create(notifications)
    return Response(serializer.data)
  return Response(serializer.errors, status = 400)

class CustomerView(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]
    def get(self, request):
        """
        Get customer detail
        ---
        parameters:
         - name: id
           type: integer
           paramType: query
           required: true
        serializer: CustomerDetailSerializer
        """

        user = request.user
        customerID = request.QUERY_PARAMS.get('id')

        try:
            customer = WxCustomer.objects.get(pk=customerID)
            serializer = CustomerDetailSerializer(customer)
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response({'msg' : "Customer doesn't exist"}, status = 404)

    def post(self, request):
        """
        Add customer to company
        ---
        serializer: CustomerSerializer
        """
        user = request.user
        # if user.company_id is None:
        #     return Response({'msg' : 'You have no company'}, status = 404)

        requestData = request.data.copy()
        requestData['sale_user'] = user.id
        customerSerializer = CustomerSerializer(data = requestData)
        if customerSerializer.is_valid():
            customerSerializer.save()

            #Add notification
            dicCustomer = customerSerializer.data
            notifyStr = '%s customer was added' % (dicCustomer['name'])
            WxNotification.createNotification(NotificationType.CUSTOMER_ADDED,
                                            user.id,
                                            notifyStr,
                                            dicCustomer).save()

            return Response(customerSerializer.data)
        return Response(customerSerializer.errors, status = 400)

    def put(self, request):
        """
        Edit customer of company
        ---
        parameters:
         - name: id
           type: integer
           required: true
        serializer: CustomerSerializer
        """
        user = request.user

        requestID = request.data.get('id', None)
        if requestID is None:
            return Response({'msg' : "Please provide customer id"}, status = 400)

        customer = WxCustomer.objects.get(pk = requestID)
        if customer.sale_user.id != user.id:
            return Response({'msg' : "You can't modify this customer"}, status = 400)

        customerSerializer = CustomerSerializer(
            customer,
            data = request.data,
            partial=True
        )

        if customerSerializer.is_valid():
            customerSerializer.save()
            return Response(customerSerializer.data)

        return Response(customerSerializer.errors, status = 400)

    def delete(self, request):
        """
        Delete customer of company
        ---
        parameters:
         - name: id
           type: integer
           paramType: query
           required: true
        responseMessages:
         - code: 400
           message: You can't delete this customer
         - code: 200
           message: Successfully deleted customer
        """
        user = request.user
        requestID = request.query_params.get('id', None)
        if requestID is None:
            return Response({'msg' : 'Please provide customer id'}, status = 400)

        customer = WxCustomer.objects.get(pk = requestID)
        if customer.sale_user.id == user.id:
            customer.delete()

            notifyStr = '%s customer was removed' % customer.name
            WxNotification.createNotification(NotificationType.CUSTOMER_REMOVED,
                                            user.id,
                                            notifyStr,
                                            None).save()
            return Response({'msg' : 'Successfully deleted customer'})
        else:
            return Response({'msg' : "You can't delete this customer"}, status = 400)

class CustomerListView(generics.ListAPIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]
    pagination_class = StandardResultsSetPagination
    serializer_class = CustomerSerializer
    def get_queryset(self):
        request = self.request
        user = self.request.user
        result = user.customer_list.all()

        cName = request.query_params.get('name', None)
        if cName != None:
            result = result.filter(name__istartswith = cName)

        cCompany = request.query_params.get('company', None)
        if cCompany != None:
            result = result.filter(company_name__istartswith = cCompany)

        cStatus = request.query_params.get('status', None)

        dateNow = datetime.datetime.now()
        datePast = dateNow + datetime.timedelta(days= -180)
        if cStatus != None:
            cStatus = cStatus.lower()
            if cStatus == 'lead':
                result = result.filter(Q(deals__stage__lt = 1004))
            elif cStatus == 'customer':
                result = result.exclude(
                    Q(deals__stage__lt = 1004)
                ).filter(
                    deals__updated_at__range = (datePast, dateNow)
                )
            elif cStatus == 'past_customer':
                result = result.exclude(
                    Q(deals__stage__lt = 1004)
                ).filter(
                    deals__updated_at__lte = datePast
                )
            else:
                return []

        cCity = request.query_params.get('city', None)
        if cCity != None:
            result = result.filter(city__istartswith = cCity)

        cCountry = request.query_params.get('country', None)
        if cCountry != None:
            result = result.filter(country__istartswith = cCountry)

        cPostcode = request.query_params.get('postcode', None)
        if cPostcode != None:
            result = result.filter(postcode__istartswith = cPostcode)

        cCreateFrom = request.query_params.get('add_from',None)
        cCreateTo = request.query_params.get('add_to', None)
        if cCreateFrom != None and cCreateTo != None:
            result = result.filter(created_date__range = (cCreateFrom, cCreateTo))

        cPriority = request.query_params.get('priority', None)
        if cPriority != None:
            result = result.filter(priority = cPriority)

        cContactFrom = request.query_params.get('contact_from', None)
        cContactTo = request.query_params.get('contact_to', None)

        if cContactFrom != None and cContactTo != None:
            result = result.filter(contact_date__range = (cContactFrom, cContactTo))
        return result
    def get(self, request, **kwargs):
        """
        Search customers
        ---
        parameters:
         - name: name
           type: string
           paramType: query
         - name: company
           type: string
           paramType: query
         - name: status
           type: string
           paramType: query
           description: status of customer-"lead", "customer", "past_customer" values are allowed
         - name: city
           type: string
           paramType: query
         - name: country
           type: string
           paramType: query
         - name: postcode
           type: string
           paramType: query
         - name: add_from
           type: dateTime
           paramType: query
           description: <a href='https://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 Specification</a><br/>Reference.<br/> <a href='http://stackoverflow.com/questions/17558859/convert-iso-8601-to-nsdate'>Convert ISO 8601 to NSDate</a>
         - name: add_to
           type: dateTime
           paramType: query
           description: <a href='https://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 Specification</a><br/>Reference.<br/> <a href='http://stackoverflow.com/questions/17558859/convert-iso-8601-to-nsdate'>Convert ISO 8601 to NSDate</a>
         - name: priority
           type: integer
           enum: [0, 1, 2]
           paramType: query
         - name: contact_from
           type: dateTime
           paramType: query
           description: <a href='https://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 Specification</a><br/>Reference.<br/> <a href='http://stackoverflow.com/questions/17558859/convert-iso-8601-to-nsdate'>Convert ISO 8601 to NSDate</a>
         - name: contact_to
           type: dateTime
           description: <a href='https://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 Specification</a><br/>Reference.<br/> <a href='http://stackoverflow.com/questions/17558859/convert-iso-8601-to-nsdate'>Convert ISO 8601 to NSDate</a>
        response_serializer: CustomerSerializer
        """
        return super(CustomerListView, self).get(self, request, **kwargs)

class CustomerFormTemplateView(views.APIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticated]
    def get(self, request):
        """
        Get list of custom form template
        ---
        response_serializer: CustomerFormTemplateSerializer
        """
        user = request.user
        templates = WxCustomerFormTemplate.objects.filter(sale_user=user)
        serializer = CustomerFormTemplateSerializer(templates, many = True)
        return Response(serializer.data)

    def post(self, request):
        """
        Add custom form template
        ---
        serializer: CustomerFormTemplateSerializer
        """
        user = request.user
        numTemplate = WxCustomerFormTemplate.objects.filter(sale_user = user).count()
        if numTemplate >=4:
            return Response({'msg' : "You can't create template anymore"}, status = 420)
            
        requestData = request.data.copy()
        requestData['sale_user'] = user.id
        serializer = CustomerFormTemplateSerializer(data = requestData)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg' : serializer.data})
        return Response(serializer.errors, status = 400)
    def put(self, request):
        """
        Change custom form template
        ---
        parameters:
         - name: id
           type: integer
           required: true
        serializer: CustomerFormTemplateSerializer
        """
        user = request.user
        requestID = request.data.get('id', None)
        requestData = request.data.copy()

        if requestID is None:
            return Response({'msg' : 'Please provide template id'}, status = 400)
        try:
            template = WxCustomerFormTemplate.objects.get(pk=requestID)
            if template.sale_user != user:
                return Response({'msg' : "You can't edit this template"}, status = 400)
            requestData.pop('sale_user')
            serializer = CustomerFormTemplateSerializer(
                template,
                data = requestData,
                partial=True
            )
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status = 400)
        except ObjectDoesNotExist:
            return Response({'msg' : 'Template was not found'}, status = 404)

    def delete(self, request):
        """
        Delete custom form template
        ---
        parameters:
         - name: id
           type: integer
           paramType: query
           required: True
        """
        user = request.user
        requestID = request.query_params.get('id', None)
        if requestID is None:
            return Response({'msg' : 'Please provide template id'}, status = 400)

        try:
            template = WxCustomerFormTemplate.objects.get(pk=requestID)
            if template.sale_user != user:
                return Response({'msg' : "You can't delete this template"}, status = 400)
            template.delete()
            return Response({'msg' : 'Successfully deleted template'})
        except ObjectDoesNotExist:
            return Response({'msg' : "Template doesn't exist"}, status = 404)

@api_view(['GET'])
@authentication_classes([ExpiringTokenAuthentication,])
@permission_classes([IsAuthenticated,])
def pushTestView(request, format = None, **kwargs):
    """
    Apply job
    ---
    type:
     msg:
      type: string
      description: Response message
    parameters:
     - name: token
       type: string
       paramType: query
       required: true
     - name: notification_type
       type: number
       paramType: query
       required: true
    """

    apns_token = request.QUERY_PARAMS.get('token', None)
    notification_type = request.QUERY_PARAMS.get('notification_type', None)

    if apns_token is None or notification_type is None:
        return Response({"msg" : "Missing required parameters"}, status = 400)
    notification_type = int(notification_type)

    try:
        notify_obj = WxNotification.objects.get(type=notification_type)
        serializer = NotificationSerializer(notify_obj)
    except Exception as ex:

        if notification_type == NotificationType.CALENDAR_EVENT:

            event = WxEvent.objects.get(pk=1)
            serializer = NotificationSerializer.createCalendarNotification(event)

        elif notification_type == NotificationType.PROFILE_PROGRESS:

            user = request.user
            serializer = NotificationSerializer.createProfileProgressNotification(
                user,
                70
            )

        elif notification_type == NotificationType.COMPANY_PROFILE_PROGRESS:

            user = request.user
            serializer = NotificationSerializer.createProfileProgressNotification(
                user,
                70
            )

        elif notification_type == NotificationType.SHARE_PROFILE:

            receipt = request.user
            sharer = WxUser.objects.get(pk = 1)
            user = WxUser.objects.get(pk = 2)
            serializer = NotificationSerializer.createShareProfileNotification(
                sharer,
                user,
                receipt
            )

        elif notification_type == NotificationType.CONTACT_REQUEST:

            contact = WxChatContact.objects.get(pk = 1)
            serializer = NotificationSerializer.createContactNotification(contact)

        elif notification_type == NotificationType.CHAT_MESSAGE:

            msg = CometchatChatroommessages.objects.get(pk = 1)
            serializer = NotificationSerializer.createMessageNotification(
                msg,
                request.user
            )

        elif notification_type == NotificationType.JOB_APPLICANT_CHANGE:

            job = WxJobPost.objects.get(pk = 1)
            serializer = NotificationSerializer.createJobStatusNotification(job, 1024)

        elif notification_type == NotificationType.PROFILE_MATCH:

            job = WxJobPost.objects.get(pk = 1)
            serializer = NotificationSerializer.createProfileMatchNotification(
                request.user,
                job,
                90
            )

    
    postiOSNotification.delay(serializer.data, [apns_token])

    return Response(serializer.data, status = 200)


# Team part views
@api_view(['POST'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated,])
def invite_team_member(request, format = None, **kwargs):
    """
    Invite user as team member. If user doesn't exist, create inactive user. 
    Invitation mail is sent to user after calling this API.
    ---
    type:
     msg:
      type: string
      description: Response message
    parameters:
     - name: email
       paramType: form
       type: string
       required: true
     - name: name
       paramType: form
       type: string
       required: true
     - name: tier
       paramType: form
       type: number
       required: true
     - name: position
       paramType: form
       type: string
       required: true
    """
    strEmail = request.POST.get('email', None)
    strName = request.POST.get('name', None)
    tier = request.POST.get('tier', None)
    position = request.POST.get('position', None)

    if (strEmail is None
        or strName is None
        or tier is None
        or position is None):
        return Response({
                'msg' : 'Missing required information'
            }, status = 400)

    #Get user's team
    userTeamID = ''
    userTier = 1
    try:
        member = WxTeamMember.objects.get(
            user = request.user,
            status = TeamMemberStatusType.ACTIVE
        )
        userTeamID = member.team_uid
        userTier = member.tier
    except ObjectDoesNotExist as ex:
        #Create team with one user
        userTeamID = str(uuid.uuid1())

        member = WxTeamMember()
        member.tier = 1 # Tier 1
        member.position = 'Team Leader' # No position by default
        member.team_uid = userTeamID
        member.user = request.user
        member.status = TeamMemberStatusType.ACTIVE
        member.save()

    #Cast type to int
    tier = int(tier)
    if(tier < userTier):
        return Response(
            {'msg' : "You can't invite user with the higher tier than you"},
            status = 400
        )

    try:
        user = WxUser.objects.get(email=strEmail)
    except ObjectDoesNotExist as ex:
        user = WxUser()
        user.email = strEmail
        user.name = strName
        user.account_status = 0
        user.save()
        
    # Check if invitee already has team
    try:
        # Get team UID of invitee
        member = WxTeamMember.objects.get(
            user = user,
            status = TeamMemberStatusType.ACTIVE
        )
        inviteeTeamID = member.team_uid

        numTeamMembers = WxTeamMember.objects.filter(
            team_uid = inviteeTeamID,
            status = TeamMemberStatusType.ACTIVE
        ).count()

        if numTeamMembers < 2: 
            # User has no team, delete all invitations the user made
            invites = WxTeamMember.objects.filter(
                inviter = user,
                status = TeamMemberStatusType.INVITED
            )
            invites.delete()
            member.delete()
        else:
            # Invitee already has his team
            return Response({'msg': 'This user already has a team'}, status = 400)
    except ObjectDoesNotExist as ex:
        pass

    # Get already existing invitation
    try:
        invite = WxTeamMember.objects.get(inviter = request.user, 
                                         status = TeamMemberStatusType.INVITED,
                                         user = user,
                                         team_uid = userTeamID
                                         )
    except ObjectDoesNotExist:
        invite = None

    # Add invitation data
    member_data = {
            'tier' : tier,
            'position' : position,
            'team_uid' : userTeamID,
            'user' : user.id,
            'inviter' : request.user.id,
            'invite_uid' : uuid.uuid4(),
            'status' : TeamMemberStatusType.INVITED
    }
    serializer = TeamMemberSerializer(invite, data = member_data)
    if serializer.is_valid():
        serializer.save()

        # TODO: Send invitation email to user
        invite_link = request.build_absolute_uri(reverse('process_team_invite'))
        accept_link = invite_link + '?' + urllib.urlencode({
                                                    'action' : 'accept',
                                                    'id' : serializer.data['invite_uid']
                                                    })
        decline_link = invite_link + '?' + urllib.urlencode({
                                                    'action' : 'decline',
                                                    'id' : serializer.data['invite_uid']
                                                    })
        mail_str = render_to_string('invite_email.html', 
                                    {
                                        'username': user.name,
                                        'accept_link': accept_link,
                                        'decline_link': decline_link
                                    })

        sendmail(settings.DEFAULT_FROM_EMAIL,
                 user.email,
                 ("%s invited you to his team" % request.user.name),
                 mail_str)

        return Response(serializer.data)
    else:
        return Response({'msg' : serializer.errors}, status = 500)


@api_view(['GET'])
@parser_classes([parsers.FormParser])
@renderer_classes([renderers.TemplateHTMLRenderer])
def process_team_invite(request, format = None, **kwargs):
    """
    Accept/Decline invitation sent by email. Parameters are hidden.
    """
    action = request.QUERY_PARAMS.get('action', None)
    invite_uid = request.QUERY_PARAMS.get('id', None)

    if action is None or invite_uid is None:
        return Response(
            {'error_msg' : "Missing required parameters"},
            template_name='team_invite_result.html'
        )

    try:
        teamMember = WxTeamMember.objects.get(invite_uid = invite_uid)

        if action == 'accept':
            teamMember.status = TeamMemberStatusType.ACTIVE
            teamMember.save()
        if action == 'decline':
            teamMember.delete()

        return Response({'action': action}, template_name = 'team_invite_result.html')
    except ObjectDoesNotExist:
        return Response(
            {'error_msg': 'This invitation is invalid'},
            template_name= 'team_invite_result.html'
        )


class TeamListView(generics.ListAPIView):
    authentication_classes = (ExpiringTokenAuthentication, )
    permission_classes = (IsAuthenticated, )
    serializer_class = TeamMemberSerializer    
    def get_queryset(self):
        try:
            member = WxTeamMember.objects.get(
                user = self.request.user,
                status = TeamMemberStatusType.ACTIVE
            )
            userTeamID = member.team_uid
            userTier = member.tier
        except ObjectDoesNotExist as ex:
            return []
        queryset = WxTeamMember.objects.filter(team_uid = userTeamID)

        name = self.request.QUERY_PARAMS.get('name', None)
        position = self.request.QUERY_PARAMS.get('position', None)
        tier = self.request.QUERY_PARAMS.get('tier', None)
        contact = self.request.QUERY_PARAMS.get('contact', None)
        address = self.request.QUERY_PARAMS.get('address', None)

        if name:
            queryset = queryset.filter(user__name__istartswith = name)
        if position:
            queryset = queryset.filter(position__istartswith = position)
        if tier:
            queryset = queryset.filter(tier = tier)
        if contact:
            pass

        if address:
            queryset = queryset.filter(user__location__istartswith = address)

        return queryset.order_by('team_uid')
    def get(self, request, **kwargs):
        """
        Get all team members. If parameters are specified, it searches members with specified criteria.
        ---
        parameters:
            - name: name
              description: Substring of name for searching
              required: false
              type: string
              paramType: query
            - name: position
              description: Substring of position for searching
              type: string
              paramType: query
              required: false
            - name: tier
              description: Tier number for searching.
              type: string
              paramType: query
              required: false
            - name: contact
              description: Substring of phone number for searching  
              type: string
              paramType: query
              required: false
            - name: address
              description: Substring of user location for searching
              type: string
              paramType: query
              required: false
        response_serializer: TeamMemberSerializer
        """
        return super(TeamListView, self).get(self, request, **kwargs)

class TeamAdminView(views.APIView):
    authentication_classes = (ExpiringTokenAuthentication, )
    permission_classes = (IsAuthenticated, )
    def put(self, request, **kwargs):
        """
        Change information of team members.
        This API is permitted only for team administrators.
        ---
        type:
         msg:
          type: string
          description: Response message
        parameters:
         - name: tier
           paramType: form
           type: number
           required: false
         - name: position
           paramType: form
           type: string
           required: false
        """
        user = request.user
        memberID = kwargs['pk']

        member_data = {}
        tier = request.data.get('tier', None)
        position = request.data.get('position', None)

        if tier != None:
          tier = int(tier)
          if tier == 1:
            return Response({'msg' : 'Tier 1 is not allowed'}, status = 401)
          member_data['tier'] = tier

        if position != None:
          member_data['position'] = position
        
        try:
            member = WxTeamMember.objects.get(pk=memberID)
        except ObjectDoesNotExist:
            return Response({'msg' : "Member was not found"}, status = 404)

        try:
            userMember = WxTeamMember.objects.get(team_uid = member.team_uid, user = user)
            if member.id != userMember.id:
              if userMember.tier != 1:
                return Response(
                    {'msg' : 'You must be team leader to do this action'},
                    status = 401
                )
            else:
              # Remove tier if the user want to change himself
              member_data.pop('tier', None)
              
        except ObjectDoesNotExist:
            return Response({'msg' : "Member is not in same team"}, status = 400)

        serializer = TeamMemberSerializer(member, data = member_data, partial = True)
        if serializer.is_valid():
          serializer.save()
          return Response({'msg' : 'Successfully changed member'}, status = 200)
        return Response(serializer.errors, status = 400)
    def delete(self, request, **kwargs):
        """
        Kick members out from team.
        This API is permitted only for team administrators.
        ---
        type:
         msg:
          type: string
          description: Response message
        """
        user = request.user
        memberID = kwargs['pk']
        try:
            member = WxTeamMember.objects.get(pk=memberID)
        except ObjectDoesNotExist:
            return Response({'msg' : "Member was not found"}, status = 404)

        try:
            userMember = WxTeamMember.objects.get(
                team_uid = member.team_uid,
                user = user
            )
            if userMember.id == memberID:
              return Response({'msg': "You can't delete yourself"}, status = 400)
            if userMember.tier != 1:
              return Response(
                  {'msg': 'You must be team leader to do this action'},
                  status = 401
              )
        except ObjectDoesNotExist:
            return Response({'msg' : "Member is not in same team"}, status = 400)
        member.delete()
        return Response({'msg' : 'Successfully deleted member'}, status = 200)

# Deal Statistics
@api_view(['GET'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def getDealStatisticsMonthly(request, format = None, **kwargs):
    """
    Get statistics of monthly deals between [from, to]<br/>
    """

    from_year = int(float(kwargs['from_year']))
    from_month = int(float(kwargs['from_month']))
    to_year = int(float(kwargs['to_year']))
    to_month = int(float(kwargs['to_month']))

    to_last_day = calendar.monthrange(to_year, to_month)[1]

    fDay = datetime.date(from_year, from_month, 1)
    tDay = datetime.date(to_year, to_month, to_last_day)

    result = {}
    response = {
      'from_date': fDay,
      'to_date': tDay,
      'statistic': result
    }

    stages = [
        DealStageType.LEAD,
        DealStageType.CONTACTED,
        DealStageType.OFFER_MADE,
        DealStageType.IN_NEGOTIATION,
        DealStageType.CLOSED
    ]

    for year in xrange(from_year,to_year+1):
      subresult = {}
      result[str(year)] = subresult
      for month in xrange(1,13):
          if (year==from_year and month < from_month):
            continue
          if (year==to_year and month > to_month):
            continue

          subresult[str(month)] = {}

          fDay = datetime.date(year, month, 1)
          lDay = datetime.date(year, month, calendar.monthrange(year, month)[1])

          deals = WxDealHistory.objects.filter(
              created_at__range = (fDay, lDay),
              deal__user = request.user
          ).values().order_by('deal_id', '-created_at')#[:1]

          for deal_state in stages:
              last_deal_id = -1
              total_value = 0
              total_count = 0
              for hist in deals:
                  if (last_deal_id == hist['deal_id']):
                    continue

                  last_deal_id = hist['deal_id']

                  if hist['stage'] != deal_state:
                    continue
                  total_count = total_count + 1
                  total_value = total_value + hist['value']
        
              subresult[str(month)][str(deal_state)] = {
                'value': total_value,
                'count': total_count
                # 'last': last_deal_id,
                # 'res': deals
              }

    return Response(response, status = 200)

# Team Performance Statistics

@api_view(['GET'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def getTeamPerformanceStatisticsMonthly(request, format = None, **kwargs):
    """
    Get statistics of monthly Team Performance between [from, to]<br/>
    """

    from_year = int(float(kwargs['from_year']))
    from_month = int(float(kwargs['from_month']))
    to_year = int(float(kwargs['to_year']))
    to_month = int(float(kwargs['to_month']))

    to_last_day = calendar.monthrange(to_year, to_month)[1]

    fDay = datetime.date(from_year, from_month, 1)
    tDay = datetime.date(to_year, to_month, to_last_day)

    result = {}
    response = {
      'from_date': fDay,
      'to_date': tDay,
      'statistic': result
    }

    res = WxTeamMember.objects.filter(user = request.user).values()
    team_uid = res[0]['team_uid'] if len(res) else 0

    res = WxTeamMember.objects.filter(team_uid = team_uid).values('user_id')
    members = []
    team_members = []
    for tm in res:
        members.append(tm['user_id'])

    serializer = TeamUserSerializer(WxUser.objects.filter(id__in=members), many=True)
    # serializer.is_valid()
    # team_members.append()

    response['team_members'] = serializer.data

    for year in xrange(from_year,to_year+1):
      subresult = {}
      result[str(year)] = subresult
      for month in xrange(1,13):
          if (year==from_year and month < from_month):
            continue
          if (year==to_year and month > to_month):
            continue

          subresult[str(month)] = {}
          sum_of_total_value = 0
          sum_of_total_count = 0
          top_member = 0
          top_total_value = 0
          top_total_count = 0

          fDay = datetime.date(year, month, 1)
          lDay = datetime.date(year, month, calendar.monthrange(year, month)[1])

          for member in members:
              deals = WxDealHistory.objects.filter(
                  created_at__range = (fDay, lDay),
                  deal__user = member,
                  stage = DealStageType.CLOSED
              ).values().order_by(
                  'deal_id', '-created_at'
              )#[:1]

              last_deal_id = -1
              total_value = 0
              total_count = 0
              for hist in deals:
                  if (last_deal_id == hist['deal_id']):
                    continue
                  last_deal_id = hist['deal_id']
                  total_count = total_count + 1
                  total_value = total_value + hist['value']

              sum_of_total_count = sum_of_total_count + total_count
              sum_of_total_value = sum_of_total_value + total_value

              if top_total_value < total_value:
                  top_total_value = total_value
                  top_total_count = total_count
                  top_member = member

          subresult[str(month)] = {
              'value': sum_of_total_value,
              'count': sum_of_total_count,
              'best_member': {
                  'user_id': top_member,
                  'value': top_total_value,
                  'count': top_total_count,
              }
          }
    



    return Response(response, status = 200)

# Customer Statistics

@api_view(['GET'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def getCustomerStatisticsMonthly(request, format = None, **kwargs):
    """
    Get statistics of monthly Customer between [from, to]<br/>
    """

    from_year = int(float(kwargs['from_year']))
    from_month = int(float(kwargs['from_month']))
    to_year = int(float(kwargs['to_year']))
    to_month = int(float(kwargs['to_month']))

    to_last_day = calendar.monthrange(to_year, to_month)[1]

    fDay = datetime.date(from_year, from_month, 1)
    tDay = datetime.date(to_year, to_month, to_last_day)

    result = {}
    response = {
      'from_date': fDay,
      'to_date': tDay,
      'statistic': result
    }

    for year in xrange(from_year,to_year+1):
      subresult = {}
      result[str(year)] = subresult
      for month in xrange(1,13):
          if (year==from_year and month < from_month):
            continue
          if (year==to_year and month > to_month):
            continue

          subresult[str(month)] = {}

          fDay = datetime.date(year, month, 1)
          lDay = datetime.date(year, month, calendar.monthrange(year, month)[1])

          total_count = WxCustomer.objects.filter(
              created_date__lte = lDay,
              sale_user = request.user
          ).values().count()

          added_count = WxCustomer.objects.filter(
              created_date__range = (fDay, lDay),
              sale_user = request.user
          ).values().count()
    
          subresult[str(month)] = {
            'total': total_count,
            'added': added_count
            # 'last': last_deal_id,
            # 'res': deals
          }


    return Response(response, status = 200)


#
@api_view(['GET'])
@renderer_classes([renderers.JSONRenderer])
def getHomeStatistics(request, format=None):
    """
    Get grid statistics for hiring and finding job
    ---

    type:
        experts_ready:
            required: true
            type: number
        avail_prof:
            required: true
            type: number
        avail_jobs:
            required: true
            type: number
        wanted_prof:
            required: true
            type: number
    """

    #ToDo: Populate statistic variables with valid numbers

    # --- Initialize variables ---
    experts_ready = 0
    avail_prof = 0
    avail_jobs = 0
    wanted_prof = 0

    # --- Populate ---
    avail_jobs = WxJobPost.objects.all().count()
    experts_ready = WxUser.objects.filter(is_freelancer = True).count()

    return Response({"experts_ready" : experts_ready,
                    "avail_prof" : avail_prof,
                    "avail_jobs" : avail_jobs,
                    "wanted_prof" : wanted_prof})

@api_view(['GET'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def getSalesHomeStatistics(request, format = None, **kwargs):
    """
    Get statistics of sales for home page.
    ---
    type:
        num_active_lead:
            required: true
            type: number
            description: Number of active leads at the current
        total_value:
            required: true
            type: number
            description: Total value of deals at the current
        num_customers:
            required: true
            type: number
            description: Number of customers at the current
        win_ratio:
            required: true
            type: string
            description: Win ratio at the current in A:B format
    """
    user = request.user
    utc_now = datetime.datetime.now(pytz.UTC)

    numCustomers = WxCustomer.objects.filter(sale_user = user).count()
    numActiveLeads = WxDeal.objects.filter(
        user = user,
        closingExpDate__gte = utc_now,
        stage = DealStageType.LEAD
    ).count()
    
    totalValue = 0
    for deal in WxDeal.objects.filter(user = user):
        totalValue = totalValue + deal.value
    
    numDeals = WxDeal.objects.filter(
        Q(user = user) & ~Q(stage = DealStageType.CLOSED)
    ).count()
    numWinDeals = WxDeal.objects.filter(user = user, 
                        stage = DealStageType.CLOSED, 
                        closingExpDate__gte = F('updated_at')).count()
    winRatioA = numDeals
    winRatioB = numWinDeals

    return Response({
            'num_active_lead' : numActiveLeads,
            'total_value' : totalValue,
            'num_customers' :  numCustomers,
            'win_ratio' : ('%d:%d' % (winRatioA, winRatioB))
        })

@api_view(['GET'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def getDealStageStatistics(request, format = None, **kwargs):
    """
    Get statistics of deal stages
    ---
    type:
        lead:
            required: true
            type: number
            description: Total value of lead created in that month
        contacted:
            required: true
            type: number
            description: Total value of contacted deals in that month
        offer_made:
            required: true
            type: number
            description: Total value of offer-made deals in that month
        in_negotiation:
            required: true
            type: string
            description: Total value of in-negotiation deals in that month
        closed:
            required: true
            type: string
            description: Total value of closed deals in that month
    """
    arrStageMapping = {
                        DealStageType.LEAD : 'lead',
                        DealStageType.CONTACTED: 'contacted',
                        DealStageType.OFFER_MADE: 'offer_made',
                        DealStageType.IN_NEGOTIATION: 'in_negotiation',
                        DealStageType.CLOSED: 'closed'
                        }

    utcnow = datetime.datetime.now(pytz.UTC)
    result = {}
    for mIdx in range(1, utcnow.month + 1):
        fDay = datetime.date(utcnow.year, mIdx, 1)
        lDay = get_last_day(fDay)

        objMonResult = {
            'lead': 0,
            'contacted': 0,
            'offer_made': 0,
            'in_negotiation': 0,
            'closed': 0
        }

        deals = WxDealHistory.objects.filter(
                    created_at__range = (fDay, lDay),
                    deal__user = request.user
                ).values(
                    'deal_id','stage'
                ).order_by(
                    'created_at'
                ).distinct().annotate(total_value = Sum('value'))


        for deal in deals:
            objMonResult[arrStageMapping[deal['stage']]] = deal['total_value']

        result[mIdx] = objMonResult

    return Response(result, status = 200)

@api_view(['GET'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def getSalesTotalStatistics(request, format = None, **kwargs):
    """
    Get statistics of monthly deals total
    """

    utcnow = datetime.datetime.now(pytz.UTC)
    result = {}
    for mIdx in range(1, utcnow.month + 1):
        fDay = datetime.date(utcnow.year, mIdx, 1)
        lDay = get_last_day(fDay)
        deals = WxDealHistory.objects.filter(
                    created_at__range = (fDay, lDay),
                    deal__user = request.user,
                    stage = DealStageType.LEAD
                ).values(
                    'deal_id'
                ).order_by(
                    '-created_at'
                ).distinct().annotate(total_value = Sum('value'))[:1]

        if len(deals) > 0:
            result[mIdx] = deals[0]['total_value']
        else:
            result[mIdx] = 0

    return Response(result, status = 200)
