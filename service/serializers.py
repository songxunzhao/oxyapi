from django.conf import settings
from rest_framework import serializers
from rest_framework import pagination

from django.db.models import Q
from django.utils.translation import ugettext as _
from django.core import exceptions
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from . import utils
from django.conf import settings
import json
import syslog
import datetime, pytz


def update_instances(model, instances, data, instance_key, data_key):
    instance_mapping = {instance.__dict__[instance_key]: instance for instance in instances}
    data_mapping = {}
    for item in data:
        key = item.get(data_key)
        if key:
            data_mapping[key] = item
        else:
            model.objects.create(**item)

    # Perform creations and updates.
    for data_id, data in data_mapping.items():
        instance = instance_mapping.get(data_id, None)
        if instance is None:
            model.objects.create(**data)
        else:
            instance.__dict__.update(**data)
            instance.save()
    
    # Perform deletions on old objects
    for instance_id, instance in instance_mapping.items():
        if instance_id not in data_mapping:
            instance.delete()


class LangSerializer(serializers.ModelSerializer):
    """
    Lang Serializer
    """
    class Meta:
        from grid.models import WxLang
        model = WxLang


class AuthSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()
    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        if email and password :
            user_request = get_object_or_404(get_user_model(), email=email)
            is_valid = user_request.check_password(password)
            if not(is_valid):
                msg = _('Unable to log in with provided credentials')
                raise exceptions.ValidationError(msg)
        else:
            msg = _('Must include email and password')
            raise exceptions.ValidationError(msg)
        return user_request


class UserLangSerializer(serializers.ModelSerializer):
    user_lang_id = serializers.IntegerField(read_only = True)
    class Meta:
        from grid.models import WxUserLang
        model = WxUserLang
        read_only_fields = ('user',)

    def to_representation(self, obj):
        rep = super(UserLangSerializer, self).to_representation(obj)
        rep['lang'] = LangSerializer(obj.lang).data
        return rep


class UserLicenceSerializer(serializers.ModelSerializer):
    user_licence_id = serializers.IntegerField(read_only = True)

    class Meta:
        from grid.models import WxUserLicence
        model = WxUserLicence
        read_only_fields = ('user',)
        

class UserSkillSerializer(serializers.ModelSerializer):
    user_skill_id = serializers.IntegerField(read_only = True)

    class Meta:
        from grid.models import WxUserSkill
        model = WxUserSkill
        read_only_fields = ('user',)
        

class UserTimelineSerializer(serializers.ModelSerializer):
    timeline_id = serializers.IntegerField(read_only = True)

    class Meta:
        from grid.models import WxUserTimeline
        model = WxUserTimeline
        read_only_fields = ('user',)

# Applicant Profile Serializer
class UserApplicantSerializer(serializers.ModelSerializer):

    skill = UserSkillSerializer(many = True, required=False)
    #Array of User's licences
    licence = UserLicenceSerializer(many = True, required=False)
    #Array of User's timelines
    timeline = UserTimelineSerializer(many = True, required=False)
    #Array of User's languages
    lang = UserLangSerializer(many=True, required=False)
    match_percent = serializers.IntegerField(read_only = True)
    class Meta:
        from grid.models import WxUser
        model = WxUser
        exclude = ['last_login', 
                'is_admin', 
                'verify_code', 
                'verify_date', 
                'password', 
                'is_freelancer', 
                'is_employer', 
                'account_status',
                'notify_event',
                'notify_option'
                ]

    def to_representation(self, obj):
        result = super(UserApplicantSerializer, self).to_representation(obj)

        if result['image_link'] and not utils.is_url(result['image_link']):
            result['image_link'] = settings.MEDIA_URL + result['image_link']

        #Calculate match percentage
        context = self.context
        job = context.get('job', None)

        if job:
            result['match_percent'] = 17
        return result


class FriendSerializer(serializers.Serializer):
    user_id = serializers.IntegerField()
    name = serializers.CharField()

    def to_representation(self, obj):
        result = {
            'user_id' : obj.id,
            'name' : obj.name,
            'image_link' : obj.image_link
        }
        if result['image_link'] and not utils.is_url(result['image_link']):
            result['image_link'] = settings.MEDIA_URL + result['image_link']

        return result

###-----------------------------------
#    Freelancer profile serializer
#    This serializer is used for sharing public user profile. 
#    This serializer is read-only.
###-----------------------------------

class FreelancerSerializer(serializers.ModelSerializer):
    skill = UserSkillSerializer(many = True, required=False)
    licence = UserLicenceSerializer(many = True, required=False)
    timeline = UserTimelineSerializer(many = True, required=False)
    lang = UserLangSerializer(many=True, required=False)

    class Meta:
        from grid.models import WxUser
        model = WxUser
        exclude = ('last_login', 'is_admin', 'password')
        read_only_fields = ('id')

###---------------------------------
#    Profile serializer.
#    This serializer is used for CRUD operation of user.
###--------------------------------- 
class ProfileSerializer(serializers.ModelSerializer):
    skill = UserSkillSerializer(many = True, required=False)
    #Array of User's licences
    licence = UserLicenceSerializer(many = True, required=False)
    #Array of User's timelines
    timeline = UserTimelineSerializer(many = True, required=False)
    #Array of User's languages
    lang = UserLangSerializer(many=True, required=False)
    #Password of user. PBKDF2 Encrypted
    password = serializers.CharField(required=False, write_only = True)
    #Completion rate of user
    comp_rate = serializers.IntegerField(read_only = True, help_text="Completion Rate" )

    image_link = serializers.CharField(help_text="base64 Encoded Image", required=False)

    class Meta:
        from grid.models import WxUser
        model = WxUser
        exclude = ('last_login', 'is_admin', 'verify_code', 'verify_date', )
        read_only_fields = ('id', 'account_status', 'image_link')
        extra_kwargs = {'password' : {'write_only' : True}}
        depth = 2

    def to_representation(self, obj):
        result = super(ProfileSerializer, self).to_representation(obj)
        result['comp_rate'] = 0

        if result['image_link'] and not utils.is_url(result['image_link']):
            result['image_link'] = settings.MEDIA_URL + result['image_link']
        return result

    def create(self, validated_data):
        from grid.models import (
            WxUser,
            WxUserLang,
            WxUserSkill,
            WxUserLicence,
            WxUserTimeline
        )

        lang_data = validated_data.pop('lang', [])
        skill_data = validated_data.pop('skill', [])
        licence_data = validated_data.pop('licence', [])
        timeline_data = validated_data.pop('timeline', [])
        
        model = WxUser.objects.create(**validated_data)
        model.set_password(validated_data.get('password', ''))
        model.save()

        #Add related data
        instances = []
        for data in lang_data:
            data['user_id'] = model.id
            instances.append(WxUserLang(**data))
        WxUserLang.objects.bulk_create(instances)

        instances = []
        for data in skill_data:
            data['user_id'] = model.id
            instances.append(WxUserSkill(**data))
        WxUserSkill.objects.bulk_create(instances)
        
        instances = []
        for data in licence_data:
            data['user_id'] = model.id
            instances.append(WxUserLicence(**data))
        WxUserLicence.objects.bulk_create(instances)

        instances = []
        for data in timeline_data:
            data['user_id'] = model.id
            instances.append(WxUserTimeline(**data))
        WxUserTimeline.objects.bulk_create(instances)

        return model

    def update(self, instance, validated_data):
        from grid.models import (
            WxUser,
            WxUserLang,
            WxUserSkill,
            WxUserLicence,
            WxUserTimeline
        )

        validated_data.pop('email', None)

        lang_data = validated_data.pop('lang', None)
        skill_data = validated_data.pop('skill', None)
        licence_data = validated_data.pop('licence', None)
        timeline_data = validated_data.pop('timeline', None)

        #Password must be updated by method
        password = validated_data.pop('password', None)
        if not(instance.is_active()) and password != None:
            instance.set_password(password)

        instance.__dict__.update(**validated_data)

        """ ----- Add related data ---- """

        # ----- lang update ----
        if lang_data != None:
            for data in lang_data:
                data['user'] = instance
            lang_instances = WxUserLang.objects.filter(user_id=instance.id)
            update_instances(WxUserLang, lang_instances, lang_data, 'user_lang_id', 'user_lang_id')
        #----------------------------------------
        
        # ----- skill update -----
        if skill_data != None:
            
            for data in skill_data:
                data['user'] = instance
            skill_instances = WxUserSkill.objects.filter(user_id=instance.id)
            update_instances(WxUserSkill, skill_instances, skill_data, 'user_skill_id', 'user_skill_id')
        #---------------------------------------

        # ----- timeline update -----
        if timeline_data != None:
            for data in timeline_data:
                data['user'] = instance
            timeline_instances = WxUserTimeline.objects.filter(user_id=instance.id)
            update_instances(WxUserTimeline, timeline_instances, timeline_data, 'timeline_id', 'timeline_id')            
        #--------------------------------------

        # ----- licence update -----
        if licence_data != None:
            for data in licence_data:
                data['user'] = instance
            licence_instances = WxUserLicence.objects.filter(user_id=instance.id)
            update_instances(WxUserLicence, licence_instances, licence_data, 'user_licence_id', 'user_licence_id')

        instance.save()
        return instance
        #--------------------------------------
class UserDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxUserDevice
        model = WxUserDevice
        
class SkillSerializer(serializers.ModelSerializer):
    child_skill = serializers.SlugRelatedField(many=True, read_only = True, slug_field = 'name')
    class Meta:
        from grid.models import WxSkills
        model = WxSkills
        read_only_fields = ('skill_id',)
        exclude = ('group', )


class EduSerializer(serializers.ModelSerializer):
    child_education = serializers.SlugRelatedField(many=True, read_only = True, slug_field = 'name')
    class Meta:
        from grid.models import WxEducation
        model = WxEducation
        read_only_fields = ('id',)
        exclude = ('group', )


class QualitySerializer(serializers.ModelSerializer):
    child_quality = serializers.SlugRelatedField(many=True, read_only = True, slug_field = 'name')
    class Meta:
        from grid.models import WxQuality
        model = WxQuality
        read_only_fields = ('id',)
        exclude = ('group', )



class JobPostSkillSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        from grid.models import WxJobPostSkill
        model = WxJobPostSkill
        read_only_fields = ('job_post', )

class JobPostEduSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        from grid.models import WxJobPostEdu
        model = WxJobPostEdu
        read_only_fields = ('job_post', )

class JobPostQualitySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        from grid.models import WxJobPostQuality
        model = WxJobPostQuality
        read_only_fields = ('job_post', )

class JobPostLangSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        from grid.models import WxJobPostLang
        model = WxJobPostLang
        read_only_fields = ('job_post', )

class JobPostSerializer(serializers.ModelSerializer):
    skill_set = JobPostSkillSerializer(many = True, required = False)
    education_set = JobPostEduSerializer(many = True, required = False)
    quality_set = JobPostQualitySerializer(many = True, required = False)
    lang_set = JobPostLangSerializer(many = True, required = False)
    type = serializers.IntegerField(help_text = "Job Type")
    level = serializers.IntegerField(help_text = "Job Level")
    expire_left = serializers.IntegerField(read_only = True)

    class Meta:
        from grid.models import WxJobPost
        model = WxJobPost

    def to_representation(self, obj):
        rep = super(JobPostSerializer, self).to_representation(obj)

        #calculate expiry days left.
        publish_date = obj.publish_date
        today = datetime.datetime.now(pytz.UTC).date()
        delta = today - publish_date
        expiry_days = obj.day_expire - delta.days
        if(expiry_days > 0):
            rep['expire_left'] = expiry_days
        else:
            rep['expire_left'] = 0

        rep['total_applicant'] = obj.applicant_list.count()
        return rep
        
    def create(self, validated_data):
        from grid.models import (
            WxJobPost,
            WxJobPostSkill,
            WxJobPostQuality,
            WxJobPostEdu,
            WxJobPostLang
        )

        skills = validated_data.pop('skill_set', [])
        educations = validated_data.pop('education_set', [])
        qualities = validated_data.pop('quality_set', [])
        langs = validated_data.pop('lang_set', [])

        job_post = WxJobPost.objects.create(**validated_data)
        for skill in skills:
            skill.pop('id', None)
            skill['job_post'] = job_post
            WxJobPostSkill.objects.create(**skill)

        for education in educations:
            education.pop('id', None)
            education['job_post'] = job_post
            WxJobPostEdu.objects.create(**education)

        for quality in qualities:
            quality.pop('id', None)
            quality['job_post'] = job_post
            WxJobPostQuality.objects.create(**quality)

        for lang in langs:
            lang.pop('id', None)
            lang['job_post'] = job_post
            WxJobPostLang.objects.create(**lang)
        return job_post

    def update(self, instance, validated_data):
        from grid.models import (
            WxJobPost,
            WxJobPostSkill,
            WxJobPostQuality,
            WxJobPostEdu,
            WxJobPostLang
        )

        skills = validated_data.pop('skill_set', None)
        educations = validated_data.pop('education_set', None)
        qualities = validated_data.pop('quality_set', None)
        langs = validated_data.pop('lang_set', None)

        instance.__dict__.update(**validated_data)


        # ----- lang update ----
        if langs:
            lang_instances = WxJobPostLang.objects.filter(job_post=instance)
            for item in langs:
                item['job_post'] = instance
            update_instances(WxJobPostLang, lang_instances, langs)
        #----------------------------------------

        # ----- education update ----
        if educations:
            edu_instances = WxJobPostEdu.objects.filter(job_post=instance)
            for item in educations:
                item['job_post'] = instance

            update_instances(WxJobPostEdu, edu_instances, educations)
        #----------------------------------------
        # ----- skill update ----
        if skills:
            skill_instances = WxJobPostSkill.objects.filter(job_post=instance)
            for item in skills:
                item['job_post'] = instance

            update_instances(WxJobPostSkill, skill_instances, skills)
        #----------------------------------------
        # ----- qualities update ----
        if qualities:
            quality_instances = WxJobPostQuality.objects.filter(job_post=instance)
            for item in qualities:
                item['job_post'] = instance

            update_instances(WxJobPostQuality, quality_instances, qualities)
        #----------------------------------------
        return instance

class HobbySerializer(serializers.ModelSerializer):
    """
    Hobby & Interest serializer
    """
    class Meta:
        from grid.models import WxHobby

        model = WxHobby

##---------------------------------------
# Notification serializers
##---------------------------------------

class NotificationSerializer(serializers.ModelSerializer):
    """
    Notification Serializer
    """
    #Notification types

    class Meta:
        from grid.models import WxNotification
        model = WxNotification

    @staticmethod
    def createContactNotification(contact):
        from grid.models import NotificationType

        user = contact.user
        opposite = contact.contact
        detail = {
            'notify_img': user.image_link,
            'request_user' : user.id
        }

        obj = {
            'type' : NotificationType.CONTACT_REQUEST,
            'receipt_id' : opposite.id,
            'comment' : user.name + ' Sent you a contact request',
            'is_read' : 0,
            'created_date' : datetime.datetime.now(pytz.UTC),
            'detail' : json.dumps(detail)
        }
        serializer = NotificationSerializer(data = obj)
        if serializer.is_valid():
            serializer.save()
        return serializer

    @staticmethod
    def createProfileProgressNotification(user, percentage):
        from grid.models import NotificationType

        detail = {
            'progress' : percentage
        }
        obj = {
            'type' : NotificationType.PROFILE_PROGRESS,
            'receipt_id' : user.id,
            'comment' : '%d%% of your profile completed' % percentage,
            'is_read' : 0,
            'created_date' : datetime.datetime.now(pytz.UTC),
            'detail' : json.dumps(detail)
        }
        serializer = NotificationSerializer(data = obj)
        if serializer.is_valid():
            serializer.save()
        return serializer
    @staticmethod
    def createCompanyProfilePrgressNotification(user, percentage):
        from grid.models import NotificationType

        detail = {
            'progress' : percentage
        }
        obj = {
            'type' : NotificationType.COMPANY_PROFILE_PROGRESS,
            'receipt_id' : user.id,
            'comment' : '%d%% of your company profile completed' % percentage,
            'is_read' : 0,
            'created_date' : datetime.datetime.now(pytz.UTC),
            'detail' : json.dumps(detail)
        }
        serializer = NotificationSerializer(data = obj)
        if serializer.is_valid():
            serializer.save()
        return serializer

    @staticmethod
    def createShareProfileNotification(user, shared_user, receipt):
        from grid.models import NotificationType

        detail = {
            'user' : {
                'id' : user.id,
                'name' : user.name,
                'image_link' : user.image_link
                },
            'shared_user' : {
                'id' : shared_user.id,
                'name' : shared_user.name,
                'image_link' : shared_user.image_link
                }
        }
        obj = {
            'type' : NotificationType.SHARE_PROFILE,
            'receipt_id' : receipt.id,
            'comment' : '{0} shared {1} profile'.format(user.name, shared_user.name),
            'is_read' : 0,
            'created_date' : datetime.datetime.now(pytz.UTC),
            'detail' : json.dumps(detail)
        }
        serializer = NotificationSerializer(data = obj)
        if serializer.is_valid():
            serializer.save()

        return serializer
    @staticmethod
    def createMessageNotification(msg, receipt):
        from grid.models import WxUser, NotificationType

        user = WxUser.objects.get(pk=msg.userid)
        detail = {
            'from' : {
                'id': user.id,
                'name' : user.name,
                'image_link' : user.image_link
            },
            'message' : {
                'id': msg.id,
                'message' : msg.message
            }
        }

        obj = {
            'type' : NotificationType.CHAT_MESSAGE,
            'receipt_id' : receipt.id,
            'comment' : '{0}: {1}'.format(user.name, msg.message),
            'is_read' : 0,
            'created_date' : datetime.datetime.now(pytz.UTC),
            'detail' : json.dumps(detail)
        }
        serializer = NotificationSerializer(data = obj)
        if serializer.is_valid():
            serializer.save()
        return serializer

    @staticmethod
    def createJobStatusNotification(job, num_cand):
        from grid.models import NotificationType

        detail = {
            'job_id' : job.id
        }
        obj = {
            'type' : NotificationType.JOB_APPLICANT_CHANGE,
            'receipt_id' : job.user.id,
            'comment' : '{0} candidates applied for {1} position'.format(num_cand, job.title),
            'is_read' : 0,
            'created_date' : datetime.datetime.now(pytz.UTC),
            'detail' : json.dumps(detail)
        }
        serializer = NotificationSerializer(data = obj)
        if serializer.is_valid():
            serializer.save()
        return serializer

    @staticmethod
    def createProfileMatchNotification(user, job, percentage):
        from grid.models import NotificationType

        detail = {
            'percentage' : percentage,
            'job_id' : job.id,
            'user_id' : user.id
        }

        obj = {
            'type' : NotificationType.PROFILE_MATCH,
            'receipt_id' : job.user.id,
            'comment' : '{0} is a {1}% match for {2}'.format(user.name, percentage, job.title),
            'is_read' : 0,
            'created_date' : datetime.datetime.now(pytz.UTC),
            'detail' : json.dumps(detail)
        }
        serializer = NotificationSerializer(data = obj)
        if serializer.is_valid():
            serializer.save()
        return serializer

    @staticmethod
    def createCalendarNotification(event):
        from grid.models import NotificationType

        detail = {
            'event_id' : event.id,
            'notify_img' : ''
        }

        obj = {
            'type' : NotificationType.CALENDAR_EVENT,
            'receipt_id' : event.user.id,
            'comment' : event.name,
            'is_read' : 0,
            'created_date' : datetime.datetime.now(pytz.UTC),
            'detail' : json.dumps(detail)
        }
        serializer = NotificationSerializer(data = obj)
        if serializer.is_valid():
            serializer.save()
        return serializer

    def to_representation(self, obj):
        ###
        # Notification types
        # 1 : Normal text notification
        # 2 : Progress notification
        # 3 : Share profile notification
        # 4 : Contact request notification
        # 5 : Team request notification
        # 6 : Message Notification
        # 7 : Calendar Notification
        ###
        result = super(NotificationSerializer, self).to_representation(obj)

        try:
            result['detail'] = json.JSONDecoder().decode(result['detail'])
        except Exception:
            result['detail'] = {}

        return result

###
# Job post template serializer
###
class JobDescTemplateSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxJobDescTemplate

        model = WxJobDescTemplate
        fields = ('id', 'name')

class JobDescTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxJobDescTemplate

        model = WxJobDescTemplate


class JobSearchCriteriaSerializer(serializers.Serializer):
    keyword = serializers.CharField(required=False, help_text='Search keyword')
    job_level = serializers.IntegerField(required=False, help_text='Job level in integer')
    location = serializers.CharField(required=False, help_text='Location string')
    skill = serializers.CharField(required=False, help_text="Skills array in json string")

    def validate_skill(self, value):
        try:
            json_object = json.loads(value)
        except ValueError, e:
            raise serializers.ValidationError("skill parameter should be json array")
        return json_object

# Serializer for CRUD operation of job application
class JobApplicationSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        from grid.models import WxJobApplicant

        model = WxJobApplicant
###
# Calendar Serializers
###
class EventMetaSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxEventMeta

        model = WxEventMeta
        read_only_fields = ('id', )

class EventSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(max_length = 20, required = False)
    email = serializers.EmailField(required = False)
    duration = serializers.IntegerField(required = False)
    candidate = serializers.ListField(child = serializers.IntegerField(min_value = 1, ))
    invites = serializers.ListField( required = False, child = serializers.IntegerField(min_value = 1,))
    url = serializers.URLField(required = False)
    event_meta = EventMetaSerializer(many = True, required = False)

    class Meta:
        from grid.models import WxEvent

        model = WxEvent
        exclude = ('detail', )

    def save(self):
        event_type = self.validated_data['type']
        detail = {}
        if event_type == 0: 
            #normal event
            detail['url'] = self.validated_data['url']
            detail['invites'] = self.validated_data['invites']
        elif event_type == 1:
            #interview event
            detail['phone'] = self.validated_data['phone']
            detail['email'] = self.validated_data['email']
            detail['duration'] = self.validated_data['duration']
            detail['candidate'] = self.validated_data['candidate']
        
        self.validated_data['detail'] = json.dumps(detail)
        if not(self.instance):
            self.create(self.validated_data)
        else:
            self.update(self.instance, self.validated_data)

    def create(self, validated_data):
        from grid.models import WxEventMeta

        event_meta = validated_data.pop("event_meta", False)
        record = super(EventSerializer, self).create(validated_data)

        # Save meta data
        meta_instances = []
        for meta in event_meta:
            meta['event'] = record
            meta_instances.append(WxEventMeta(**meta))

        WxEventMeta.objects.bulk_create(meta_instances)
        return record

    def update(self, instance, validated_data):
        from grid.models import WxEventMeta

        event_meta = validated_data.pop("event_meta", False)
        record = super(EventSerializer, self).update(instance, validated_data)
        # Update meta data
        meta_instances = instance.event_meta
        update_instances(WxEventMeta, meta_instances, event_meta, 'id', 'id')

        return record 
        
    def to_representation(self, obj):
        result = super(CalendarSerializer, self).to_representation(obj)

        event_type = obj.type
        detail = json.JSONDecoder.decode(obj.detail)

        if event_type == 0:
            result['url'] = detail.url
            result['invites'] = detail.invites
        elif event_type == 1:
            result['phone'] = detail.phone
            result['email'] = detail.email
            result['duration'] = detail.duration
            result['candidate'] = detail.candidate
        return result


class JobTitleSampleSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxJobTitleSample

        model = WxJobTitleSample
        read_only_fields = ('id',)


class JobFieldSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxJobField

        model = WxJobField
        read_only_fields = ('id',)


class CompanyMarketSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxCompanyMarket

        model = WxCompanyMarket
        read_only_fields = ('id',)


class CompanyMemberSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxCompanyMember

        model = WxCompanyMember
        read_only_fields = ('id',)


class CompanyServiceSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxCompanyService

        model = WxCompanyService
        read_only_fields = ('id',)


class CompanyBenefitSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxCompanyBenefit

        model = WxCompanyBenefit
        read_only_fields = ('id',)


class CompanySerializer(serializers.ModelSerializer):
    market_list = CompanyMarketSerializer(many=True, read_only = True)
    member_list = CompanyMemberSerializer(many=True, read_only = True)
    service_list = CompanyServiceSerializer(many=True, read_only = True)
    benefit_list = CompanyBenefitSerializer(many=True, read_only = True)

    class Meta:
        from grid.models import WxCompany

        model = WxCompany
        read_only_fields = ('id',)


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        from grid.models import WxTask
        model = WxTask
        read_only_fields = ('id', )


class ChatContactSerializer(serializers.ModelSerializer):

    class Meta:
        from grid.models import WxChatContact

        model = WxChatContact
        read_only_fields = ('id')

    def to_representation(self, obj):
        result = super(ChatContactSerializer, self).to_representation(obj)
        serializer = ProfileSerializer(obj.contact)
        result['contact'] = serializer.data
        return result


class CustomerFormTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxCustomerFormTemplate

        model = WxCustomerFormTemplate
        read_only_fields = ('id',)


class CustomerSerializer(serializers.ModelSerializer):
    type = serializers.CharField(max_length = 20, read_only = True)

    class Meta:
        from grid.models import WxCustomer

        model = WxCustomer
        read_only_fields = ('id', 'created_date',)

    def to_representation(self, obj):
        result = super(CustomerSerializer, self).to_representation(obj)

        # Determine type
        bExistOpenDeal = obj.deals.filter(~Q(stage=1004)).exists()
        if bExistOpenDeal:             
            result['type'] = 'Lead'
        else:
            dateNow = datetime.datetime.now()
            datePast = dateNow + datetime.timedelta(days= -180)
            bExistOldDeal = obj.deals.filter(updated_at__range = (datePast, dateNow)).exists()
            if bExistOldDeal:
                result['type'] = 'Customer'
            else:
                result['type'] = 'Past customer'

        # Calculate total revenue
        objDeals = obj.deals.filter(stage = 1004)
        numRevenue = 0
        for deal in objDeals:
            numRevenue = numRevenue + deal.value
        result['revenue'] = numRevenue
        return result

    def validate(self, data):
        request = self.context.get('request', None)
        if(request):
            data['sale_user'] = request.user
        return data


class DealSerializer(serializers.ModelSerializer):
    def validate(self, data):
        request = self.context.get('request', None)
        if(request):
            data['user'] = request.user
        return data

    def to_representation(self, obj):
        result = super(DealSerializer, self).to_representation(obj)
        customerObj = CustomerSerializer(obj.customer)
        result["customer_data"] = customerObj.data
        return result

    class Meta:
        from grid.models import WxDeal
        model = WxDeal


class DealHistorySerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxDealHistory
        model = WxDealHistory
        

class DealInvoiceProductSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxDealInvoiceProduct
        model = WxDealInvoiceProduct


class DealInvoiceSerializer(serializers.ModelSerializer):
    products = DealInvoiceProductSerializer(many=True, read_only = True)

    class Meta:
        from grid.models import WxDealInvoice
        model = WxDealInvoice


class RevenueHistorySerializer(serializers.Serializer):
    date = serializers.DateField()
    revenue = serializers.IntegerField()


class CustomerDetailSerializer(serializers.ModelSerializer):
    deals = DealSerializer(many=True, read_only = True)
    revenue_history = RevenueHistorySerializer(many=True, read_only = True)
    
    class Meta:
        from grid.models import WxCustomer
        model = WxCustomer
        read_only_fields = ('id', 'created_date',)

    def to_representation(self, obj):
        result = super(CustomerDetailSerializer, self).to_representation(obj)

        deals = obj.deals.all()[:5]
        serializer = DealSerializer(deals, many=True)
        result['deals'] = serializer.data

        arrRevenue = []
        deals = obj.deals.filter(stage = 1004).order_by('-updated_at')
        for deal in deals:
            strUpdateDate = deal.updated_at.strftime('%Y-%m-%d')
            if len(arrRevenue) == 0:
                arrRevenue.append({
                    'date': strUpdateDate,
                    'revenue' : deal.value
                    })
            else:
                if arrRevenue[-1].date == strUpdateDate:
                    arrRevenue[-1].revenue += deal.value
                else:
                    arrRevenue.append({
                        'date' : strUpdateDate,
                        'revenue' : deal.value
                        })
        result['revenue_history'] = arrRevenue
        return result


class TeamUserSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxUser

        model = WxUser
        fields = ('id', 
                'name', 
                'email', 
                'image_link', 
                'location', 
                'account_status', 
                'contact_site', )
        read_only_fields = ('id',)


class TeamMemberSerializer(serializers.ModelSerializer):
    class Meta:
        from grid.models import WxTeamMember

        model = WxTeamMember
        read_only_fields = ('id', )

    def to_representation(self, obj):
        result = super(TeamMemberSerializer, self).to_representation(obj)
        serializer = TeamUserSerializer(obj.user)
        result['user'] = serializer.data
        return result


class ChangeUserPasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(min_length=6)
    new_password = serializers.CharField(min_length=6)
