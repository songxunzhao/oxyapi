import os
import json
import urllib2
import datetime
from django.core.exceptions import ObjectDoesNotExist


def is_url(path):
	if path.startswith('http://') or path.startswith('https://'):
		return True
	else:
		return False


def remove_duplicates(values):
	output = []
	seen = set()
	for value in values:
		# If value has not been encountered yet,
		# ... add it to both list and set.
		if value not in seen:
			output.append(value)
			seen.add(value)
	return output


def get_usd_exchange_rate(value_currency):

	try:
		result = json.load(urllib2.urlopen("http://api.fixer.io/latest?base=USD"))
		return 1 / result['rates'][value_currency]
	except:
		return -1


def get_first_day(dt, d_years=0, d_months=0):
	# d_years, d_months are "deltas" to apply to dt
	y, m = dt.year + d_years, dt.month + d_months
	a, m = divmod(m-1, 12)
	return datetime.date(y+a, m+1, 1)


def get_last_day(dt):
	return get_first_day(dt, 0, 1) + datetime.timedelta(-1)


def get_model_obj(model, **kwargs):
	try:
		return model.objects.get(**kwargs)
	except ObjectDoesNotExist:
		return None