import smtplib
import grid.settings as settings

# Import the email modules we'll need
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def sendmail(fromAddr, toAddr, subject, mailText):
	msg = MIMEMultipart()
	msg['Subject'] = subject
	msg['From'] = fromAddr
	msg['To'] = toAddr

	part = MIMEText(mailText, 'html')
	msg.attach(part)

	# Send the message via our own SMTP server, but don't include the
	# envelope header.
	s = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
	s.starttls()
	s.login(settings.EMAIL_USERNAME, settings.EMAIL_PASSWORD)

	result = False
	try:
		s.sendmail(fromAddr, [toAddr], msg.as_string())
		result = True
	except Exception as ex:
		result = False

	s.quit()

	return result