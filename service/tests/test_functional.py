from django.test import TestCase
from service.views import (
	change_password,
	signup,
	invite_team_member,
	process_team_invite
)
from grid.models import WxUser
from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate

from service.utils import get_model_obj


# Create your tests here.
class APITest(TestCase):
	def setUp(self):
		factory = APIRequestFactory()
		request = factory.post(
			'/service/user/signup',
			{
				'email': 'songxunzhao1991@gmail.com',
				'name': 'songxunzhao',
				'password': 'password'
			}
		)
		response = signup(request)

		self.assertEqual(response.status_code, 200)

		request = factory.post(
			'/service/user/signup',
			{
				'email': 'blabla@gmail.com',
				'name': 'blabla',
				'password': 'password'
			}
		)
		response = signup(request)

		self.user = WxUser.objects.get(email='songxunzhao1991@gmail.com')

	def test_change_password(self):
		print "\nTestCase: Change user password"

		factory = APIRequestFactory()

		request = factory.post(
			'/service/user/password/change',
			{'old_password': 'password', 'new_password': 'blabla'},
			format='json'
		)

		force_authenticate(request, user=self.user)

		response = change_password(request)

		self.assertEqual(response.status_code, 200)

	def test_invite_new_member(self):
		print "\nTestCase: Invite unregistered user & accept invitation"

		factory = APIRequestFactory()

		request = factory.post(
			'/service/team/member/invite',
			{
				'email': 'songxunzhao1991@163.com',
				'name': 'blabla',
				'tier': 1,
				'position': 'Team member'
			}
		)
		force_authenticate(request, user=self.user)

		response = invite_team_member(request)

		self.assertEqual(response.status_code, 200)

		new_member_user = get_model_obj(WxUser, email='songxunzhao1991@163.com')

		self.assertIsNotNone(new_member_user, "New user wasn't created")

		new_member = response.data

		request = factory.get(
			'/service/team/member/invitations',
			{
				'action': 'accept',
				'id': new_member.get('invite_uid')
			}
		)

		response = process_team_invite(request)

		self.assertEqual(response.data, {'action': 'accept'})

	def test_invite_member(self):
		print "\nTestCase: Invite registered user & accept invitation"

		factory = APIRequestFactory()

		request = factory.post(
			'/service/team/member/invite',
			{
				'email': 'blabla@gmail.com',
				'name': 'blabla',
				'tier': 1,
				'position': 'Team member'
			}
		)
		force_authenticate(request, user=self.user)

		response = invite_team_member(request)

		self.assertEqual(response.status_code, 200)

		new_member_user = get_model_obj(WxUser, email='blabla@gmail.com')

		self.assertIsNotNone(new_member_user, "New user wasn't created")

		new_member = response.data

		request = factory.get(
			'/service/team/member/invitations',
			{
				'action': 'accept',
				'id': new_member.get('invite_uid')
			}
		)

		response = process_team_invite(request)

		self.assertEqual(response.data, {'action': 'accept'})