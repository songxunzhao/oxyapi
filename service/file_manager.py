"""
	Implement image upload handler.
	Created by ZSX
"""
import os
from django.conf import settings
import time
import random


def handle_uploaded_file(f):
	path = settings.UPLOAD_PATH

	filename, fileext = os.path.splitext(f.name);
	if fileext != '.jpeg' and fileext !='.jpg' and fileext != '.png':
		raise Exception('This file type was not supported')

	ts = time.time()

	filename = str(random.randint(1, 100)) + str(int(ts)) + fileext
	filepath = path + filename

	destination = open(filepath, 'wb+')
	for chunk in f.chunks():
		destination.write(chunk)
	destination.close()

	return filename


def handle_base64_file(imgdata, fileext):
	try:
		path = settings.UPLOAD_PATH
		ts = time.time()
		filename = str(random.randint(1, 100)) + str(int(ts)) + fileext
		filepath = path + filename
		destination = open(filepath, 'wb+')
		destination.write(imgdata.decode('base64'))
		destination.close()
	except Exception as ex:
		return None

	return filename