from django.conf.urls import patterns, include, url
from rest_framework import routers, serializers, viewsets
from views import *


#APIs
router = routers.DefaultRouter()
router.register(r'skill', Skill, base_name='skill')
router.register(r'education', Education, base_name='education')
router.register(r'quality', Quality, base_name='quality')
router.register(r'lang',LangViewSet, base_name='lang_list')
router.register(r'job/desc_template', JobDescTemplateView, base_name='desc_template')
router.register(r'user/contact', ChatContactView, base_name = 'chat_contact')

urlpatterns = patterns('',
                       # Examples:
    # url(r'^$', 'grid.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include(router.urls)),
                       url(r'^user/(?P<pk>[0-9]+)$', UserDetail.as_view()),
                       url(r'^user/verify/(?P<email>[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)/(?P<verify_code>[^/.]+)$', verify_signup, name = 'verify_signup'),
                       url(r'^user/auth$', auth, name = 'auth'),
                       url(r'^user/social_auth$', socialAuth),
                       url(r'^user/signup$', signup),

                       url(r'^user/password/request_recover$', request_recover),
                       url(r'^user/password/change$', change_password),
                       url(r'^user/password/verify_request$', verify_request),
                       url(r'^user/password/recover$', recover_password),

                       url(r'^user/profile/$', Profile.as_view()),
                       url(r'^user/profile/lang$', ProfileLanguage.as_view()),
                       url(r'^user/profile/skill$', ProfileSkill.as_view()),
                       url(r'^user/profile/licence$', ProfileLicence.as_view()),
                       url(r'^user/profile/timeline$', ProfileTimeline.as_view()),
                       url(r'^user/friend/list/$', FriendListView.as_view()),
                       url(r'^user/friend/list/(?P<keyword>[^/.]*)/$', FriendListView.as_view()),
                       url(r'^user/friend/(?P<pk>[0-9]+)/$', FriendOperationView.as_view()),

                       # url(r'^user/event/$', EventView.as_view()),
    url(r'^user/search$', UserList.as_view()),

                       url(r'^user/task$', DashboardTask.as_view()),
                       url(r'^user/task/list$', DashboardTaskList.as_view()),

                       url(r'^hobby/(?P<keyword>.+)/$', HobbySearchView.as_view()),
                       url(r'^hobby/$', HobbyListView.as_view()),

                       # Job related APIs
    url(r'^job/post$', JobPostView.as_view()),
                       url(r'^job/search$', JobSearchView.as_view()),
                       url(r'^job/template/$', JobTemplateView.as_view()),

                       url(r'^job/(?P<pk>[0-9]+)/$', JobDetailView.as_view()),
                       url(r'^job/(?P<pk>[0-9]+)/applicants/$', JobApplicantsView.as_view()),

                       url(r'^job/(?P<pk>[0-9]+)/apply$', jobApplyView),
                       url(r'^job/application/(?P<pk>[0-9]+)/retract$', jobRetractView),
                       url(r'^job/opened/', OpenJobView.as_view()),
                       url(r'^job/title/fields/', JobFieldView.as_view()),
                       url(r'^job/title/samples/', JobTitleSampleView.as_view()),

                       url(r'^company/profile/$', CompanyProfile.as_view()),
                       url(r'^company/market/$', CompanyMarket.as_view()),
                       url(r'^company/benefit/$', CompanyBenefit.as_view()),
                       url(r'^company/member/$', CompanyMember.as_view()),
                       url(r'^company/service/$', CompanyService.as_view()),

                       url(r'^deal/filter/$', DealFilterView.as_view()),
                       url(r'^deal/list/$', DealListView.as_view()),
                       url(r'^deal/$', DealView.as_view()),
                       url(r'^deal/batch$', create_deal_batch),

                       url(r'^deal/(?P<pk>[0-9]+)/invoice$', DealInvoiceView.as_view()),
                       url(r'^deal/(?P<pk>[0-9]+)/invoice/product$', DealInvoiceProductView.as_view()),
                       url(r'^deal/(?P<pk>[0-9]+)/invoice/product/(?P<ppk>[0-9]+)$', DealInvoiceProductDeleteView.as_view()),

                       url(r'^customer/list/$', CustomerListView.as_view()),
                       url(r'^customer/$', CustomerView.as_view()),
                       url(r'^customer/batch$', create_customer_batch),
                       url(r'^customer/templates/$', CustomerFormTemplateView.as_view()),

                       url(r'^team/member/invite$', invite_team_member),
                       url(r'^team/member/list$', TeamListView.as_view()),
                       url(r'^team/member/invitation$', process_team_invite, name ='process_team_invite'),
                       url(r'^team/member/(?P<pk>[0-9]+)/$', TeamAdminView.as_view()),

                       url(r'^notification/$', NotificationListView.as_view()),
                       url(r'^notification/sales/$', SalesNotificationView.as_view()),

                       #Deals statistics API
    url(r'^statistics/deals/(?P<from_year>[0-9]{4})/(?P<from_month>[0-9]{1,2})/(?P<to_year>[0-9]{4})/(?P<to_month>[0-9]{1,2})/$', getDealStatisticsMonthly),

                       #Team Performance statistics API
    url(r'^statistics/team/performance/(?P<from_year>[0-9]{4})/(?P<from_month>[0-9]{1,2})/(?P<to_year>[0-9]{4})/(?P<to_month>[0-9]{1,2})/$', getTeamPerformanceStatisticsMonthly),

                       #Customers statistics API
    url(r'^statistics/customers/(?P<from_year>[0-9]{4})/(?P<from_month>[0-9]{1,2})/(?P<to_year>[0-9]{4})/(?P<to_month>[0-9]{1,2})/$', getCustomerStatisticsMonthly),


                       #Sales statistics API
    url(r'^statistics/home$', getHomeStatistics),
                       url(r'^statistics/sales/home$', getSalesHomeStatistics),
                       url(r'^statistics/sales/deal_stage$', getDealStageStatistics),
                       url(r'^statistics/sales/total$', getSalesTotalStatistics),
                       # url(r'^sales/dashboard$', getSalesDashboard),

    url(r'^push_test$', pushTestView),
                       )
