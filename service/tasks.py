from __future__ import absolute_import
import requests
from apnsclient import *
import OpenSSL
from django.conf import settings
from celery import shared_task

from grid.models import DeviceType, WxUserDevice
from .utils import *

@shared_task()
def postChatMessage(payload):
	requests.post(settings.CHAT_SERVICE_URL, data = payload)

@shared_task()
def postiOSNotification(payload, tokens):

	tokens = remove_duplicates(tokens)

	#Msgs
	alert_msg = payload['comment']
	extra_msg = {
		'extra' : payload
	}

	#Disable SSLv3 Authentication
	OpenSSL.SSL.SSLv3_METHOD = OpenSSL.SSL.TLSv1_METHOD

	#Open sandbox connection
	session = Session()
	con = session.get_connection(
		'push_' + settings.APPLE_PUSH_MODE,
		cert_file=settings.BASE_DIR + "/assets/pem/" + settings.PEM_FILE
	)
	message = Message(tokens, alert=alert_msg, badge=1, extra=extra_msg)
	srv = APNs(con)
	try:
		res = srv.send(message)
	except Exception as ex:
		print "Can't connect to APNs, Exception: " + str(ex)
		session.shutdown()
	else:
		failed_tokens = []
		for token, reason in res.failed.items():
			code, errmsg = reason
			print "Device failed: {0}, reason: {1}".format(token, errmsg)

		for code, errmsg in res.errors:
			print "Error: {}".format(errmsg)

		if res.needs_retry():
			res.retry()

	session.shutdown()

@shared_task()
def postNotification(payload, devices):
	# Push notification to apple devices

	# Extract iOS devices from device list
	ios_devices = []
	for device in devices:
		if(device.device_type == DeviceType.IOS_DEVICE):
			ios_devices.append(device.reg_token)

	ios_devices = remove_duplicates(ios_devices)

	#Msgs
	alert_msg = payload['comment']
	extra_msg = {
		'extra' : payload
	}

	#Disable SSLv3 Authentication
	OpenSSL.SSL.SSLv3_METHOD = OpenSSL.SSL.TLSv1_METHOD

	#Open sandbox connection
	session = Session()
	con = session.get_connection(
		'push_' + settings.APPLE_PUSH_MODE,
		cert_file=settings.BASE_DIR + "/assets/pem/" + settings.PEM_FILE
	)
	message = Message(ios_devices, alert=alert_msg, badge=1, extra=extra_msg)
	srv = APNs(con)
	try:
		res = srv.send(message)
	except Exception as ex:
		print "Can't connect to APNs, Exception: " + str(ex)
		session.shutdown()
	else:
		failed_tokens = []
		for token, reason in res.failed.items():
			code, errmsg = reason
			print "Device failed: {0}, reason: {1}".format(token, errmsg)

		for code, errmsg in res.errors:
			print "Error: {}".format(errmsg)

		if res.needs_retry():
			res.retry()

	session.shutdown()

@shared_task()
def removeInvalidDevices():
	#Disable SSLv3 Authentication
	OpenSSL.SSL.SSLv3_METHOD = OpenSSL.SSL.TLSv1_METHOD

	con = Session().new_connection(
		'feedback_' + settings.APPLE_PUSH_MODE,
		cert_file = settings.BASE_DIR + "/assets/pem/" + settings.PEM_FILE
	)
	service = APNs(con)
	try:
		for token in service.feedback():
			WxUserDevice.objects.filter(reg_token = token).delete()
	except:
		print "removeInvalidDevices: Can't connect to APNs, seems that network is down"