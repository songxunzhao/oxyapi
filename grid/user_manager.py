"""
    Implement Custom user manager.
    Created by JSH

"""

from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


##  Define custom user manager class
##  This class is used with WxUser class for managing Django user authentication and authorization

class WxUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=WxUserManager.normalize_email(email),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        u = self.create_user(email,
                        password=password
                    )
        u.is_admin = True
        u.save(using=self._db)
        return u
