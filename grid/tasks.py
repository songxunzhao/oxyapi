from __future__ import absolute_import

from celery import task,shared_task


@task()
def add(x, y):
	return x + y


@shared_task
def mul(x, y):
    return x * y


@shared_task
def xsum(numbers):
    return sum(numbers)