# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from .common_fields import *
from django.db import models
from django_enumfield import enum
from .user_manager import *
from django.core.exceptions import *
from model_utils import Choices
import json, datetime, pytz, decimal

# Enum classes
class NotificationType(enum.Enum):
    CALENDAR_EVENT = 0
    PROFILE_PROGRESS = 1
    COMPANY_PROFILE_PROGRESS = 2
    SHARE_PROFILE = 3
    CONTACT_REQUEST = 4
    CHAT_MESSAGE = 5
    JOB_APPLICANT_CHANGE = 6
    PROFILE_MATCH = 7
    JOB_APPLIED = 8
    JOB_EXPIRED = 9
    JOB_REJECTED = 10
    JOB_ACCEPTED = 11

    #Must add sales notification types in this section.
    DEAL_ADDED = 12
    DEAL_STATUS_CHANGED = 13
    DEAL_CLOSED = 14
    CUSTOMER_ADDED = 15
    CUSTOMER_REMOVED = 16
    TASK_ADDED = 17
    INVOICE_SENT = 18
    DEAL_BEFORE_EXPIRE = 19
    DEAL_EXPIRED = 20


class DeviceType(enum.Enum):
    IOS_DEVICE = 0
    ANDROID_DEVICE = 1

class JobType(enum.Enum):
    FULL_TIME = 0
    PART_TIME = 1
    CONTRACT = 2
    FREELANCER = 3

class JobLevel(enum.Enum):
    INTERNSHIP = 0
    ENTRY_LEVEL = 1
    MID_LEVEL = 2
    SENIOR_LEVEL = 3

class JobNotifyWhen(enum.Enum):
    EVERY_APPLICATION = 0
    ONCE_DAY = 1
    ONCE_WEEK = 2

class JobNotifyAbout(enum.Enum):
    ABOUT_EVERYONE = 0
    BEST_MATCH = 1
    EXP_CAND = 2

class SalaryType(enum.Enum):
    YEARLY = 0
    MONTHLY = 1
    HOURLY = 2

class DealStageType(enum.Enum):
    LEAD = 1000
    CONTACTED = 1001
    OFFER_MADE = 1002
    IN_NEGOTIATION = 1003
    CLOSED = 1004
    @staticmethod
    def toString(typeVal):
        dicNotificationStr = {
            DealStageType.LEAD: 'lead', 
            DealStageType.CONTACTED: 'contacted', 
            DealStageType.OFFER_MADE: 'offer made', 
            DealStageType.IN_NEGOTIATION: 'in negotiation', 
            DealStageType.CLOSED: 'closed'
        }
        return dicNotificationStr[typeVal]

class TeamMemberStatusType(enum.Enum):
    INVITED = 0
    ACTIVE = 1

# Default decimal json encoder
def decimal_default(obj):
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    raise TypeError
# Models
class WxActivity(models.Model):
    id = models.CharField(primary_key=True, max_length=255)

    class Meta:
        db_table = 'wx_activity'


class WxUser(AbstractBaseUser):
    # User's name
    name = models.CharField(max_length=256, blank=True)

    # User's major job
    major = models.CharField(max_length=255, blank=True,
        help_text = "User's major job")

    # User's email
    email = models.CharField(unique=True, max_length=255)

    # User's avatar image link
    image_link = models.CharField(max_length=1024, null=True)

    # User's profile type - Freelancer, Part-time, full-time
    type = models.IntegerField(default = 0)

    # User's experienced years
    year_exp = models.IntegerField(default = 0)
    is_freelancer = models.BooleanField(default=False)
    is_employer = models.BooleanField(default=False)

    # Hourly rate
    hourly_rate = models.DecimalField(default=0, max_digits=10, decimal_places=1)

    # Hourly rate currency
    currency_type = models.CharField(max_length=8, blank=True, default = 'USD')

    # IsRelocate
    relocate = models.BooleanField(default=False)

    # User's hobby in json array
    hobby = models.TextField(blank=True)

    # User's location in string
    location = models.CharField(max_length=256, blank=True)

    # User's bio(description)
    bio = models.TextField(blank=True)

    # User's portfolio links in json array
    portfolio = models.TextField(blank=True)

    # User's contact site links in json array
    contact_site = models.TextField(blank=True)

    # Joined date when user joined into this system
    joined_date = models.DateTimeField(blank=True, null=True)

    # Notify event
    notify_event = models.BooleanField(default=False)

    # Notify option
    notify_option = models.BooleanField(default=False)

    # Is administrator, administrator of system
    is_admin = models.BooleanField(default=False)

    # User's account status, 0: Not verified, 1: active, 2: suspended, 3: closed
    account_status = models.IntegerField(default = 0)

    verify_code = models.CharField(max_length=64, blank=True)
    verify_date = models.DateTimeField(blank=True, null=True)

    objects = WxUserManager()
    USERNAME_FIELD = 'email'

    class Meta:
        db_table = 'wx_user'
        
    def get_full_name(self):
        # The user is identified by their email address
        return self.email
    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True
    def is_active(self):
        if (self.account_status == 1) or (self.account_status == 2):
            return True
        else:
            return False

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


class WxEvent(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    user = models.ForeignKey(WxUser, 
                            null = True, 
                            help_text="User who created this event")
    name = models.CharField(max_length=256, 
                            blank=True, 
                            help_text = "Event name")
    start = models.DateTimeField(blank=True, 
                                null=True, 
                                help_text = "The date when the event starts")
    end = models.DateTimeField(blank=True, 
                                null=True, 
                                help_text = "The date when the event ends")
    end_repeat = models.DateField(blank=True, 
                                null= True,
                                help_text = "The date when the event repeat ends")

    # Type of event - 0 for normal event, 1 for invite, 2 for birthday.
    type = models.IntegerField(help_text = "Type of event. 0 for normal event, 1 for interview")
    location = models.CharField(max_length=1024, 
                                blank=True, 
                                help_text="The location where the event take place")
    lat = models.DecimalField(default = 0, 
                            max_digits = 5, 
                            decimal_places = 2, 
                            help_text = "Latitude of the location")
    lon = models.DecimalField(default = 0, 
                            max_digits = 5, 
                            decimal_places = 2,
                            help_text = "Longitude of the location")
    need_alert = models.BooleanField(default = False,
                                    help_text ="Neet alert notification?")
    alert_before = models.IntegerField(default = 0,
                                    help_text = "Time delta between notification and event")
    note = models.CharField(max_length=1024, 
                            blank=True,
                            help_text = "Note for event")
    detail = models.TextField(default = "",
                            help_text = "Detail for event. Detail can vary according to event")
    
    class Meta:
        db_table = 'wx_event'

class WxEventMeta(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    event = models.ForeignKey(WxEvent, null=True, related_name = "event_meta")
    repeat_type = models.CharField(max_length = 10)
    repeat_start = models.DateTimeField()
    repeat_interval = models.IntegerField(null = True)
    repeat_year = models.IntegerField(null = True)
    repeat_month = models.IntegerField(null = True)
    repeat_day = models.IntegerField(null = True)
    repeat_week = models.IntegerField(null = True)
    repeat_weekday = models.IntegerField(null = True)
    
    class Meta:
        db_table = 'wx_event_meta'

class WxCompany(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.OneToOneField(WxUser, related_name = 'company')
    logo = models.CharField(max_length=1024, blank=True)
    name = models.CharField(max_length=255, blank=True)
    industry = models.CharField(max_length=255, blank=True)
    location = models.CharField(max_length=255, blank=True)
    about_tag = models.TextField(blank=True)
    about_desc = models.TextField(blank=True)
    product_desc = models.CharField(max_length=255, blank = True)
    office = models.TextField(blank=True)

    portfolio_links = models.TextField(blank=True)

    class Meta:
        db_table = 'wx_company'


class WxCompanyMarket(models.Model):
    id = models.AutoField(primary_key=True)
    company = models.ForeignKey(WxCompany, blank=True, null=True, related_name='market_list')
    market_type = models.ForeignKey('WxMarketType', blank=True, null=True)

    class Meta:
        db_table = 'wx_company_market'


class WxCompanyMember(models.Model):
    id = models.AutoField(primary_key=True)
    company = models.ForeignKey(WxCompany, blank=True, null=True, related_name='member_list')
    user_id = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'wx_company_member'


class WxCompanyService(models.Model):
    id = models.AutoField(primary_key=True)
    company = models.ForeignKey(WxCompany, blank=True, null=True, related_name='service_list')
    service_type = models.IntegerField(blank=True, null=True)
    appstore_link = models.CharField(max_length=1024, blank=True)
    playstore_link = models.CharField(max_length=1024, blank=True)
    market_link = models.CharField(max_length=1024, blank=True)
    website_link = models.CharField(max_length=1024, blank=True)
    imgs = models.TextField(blank=True)

    class Meta:
        db_table = 'wx_company_service'

class WxCompanyBenefit(models.Model):
    id = models.AutoField(primary_key = True)
    company = models.ForeignKey(WxCompany, blank=True, null=True, related_name='benefit_list')
    benefit_type= models.CharField(max_length=256)

    class Meta:
        db_table = 'wx_company_benefit'

class WxTask(models.Model):
    id = models.AutoField(primary_key = True)
    user = models.ForeignKey(WxUser, related_name = 'task_list')
    desc = models.CharField(max_length = 250, blank = True)
    is_checked = models.BooleanField()
    created_date = models.IntegerField()
    class Meta:
        db_table = 'wx_task'

class WxContract(models.Model):
    id = models.IntegerField(primary_key=True)
    job_post_id = models.IntegerField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    status = models.ForeignKey('WxContractStatusType', db_column='status', blank=True, null=True)

    class Meta:
        db_table = 'wx_contract'

class WxChatContact(models.Model):
    id = BigAutoField(primary_key=True)
    user = models.ForeignKey('WxUser', null=True, db_column='user_id', related_name = 'chatContacts')
    contact = models.ForeignKey('WxUser', null=True, db_column = 'contact_id')
    status = models.IntegerField(default = 0)

    class Meta:
        db_table = 'wx_chat_contact'
        
class WxContractStatusType(models.Model):
    id = models.IntegerField(primary_key=True)
    status_name = models.CharField(max_length=255, blank=True)

    class Meta:
        db_table = 'wx_contract_status_type'

class WxEducation(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=128, blank=True)
    group = models.ForeignKey('self', blank=True, null=True, related_name='child_education')

    class Meta:
        db_table = 'wx_education'

class WxFriendship(models.Model):
    id = BigAutoField(primary_key=True)
    firstuser = models.ForeignKey('WxUser', null=True, db_column='firstUser',related_name='firstFriends')  # Field name made lowercase.
    seconduser = models.ForeignKey('WxUser', null=True, db_column='secondUser',related_name='secondFriends')  # Field name made lowercase.
    type = models.IntegerField(blank=True, null=True, help_text = "Type of friendship, 0 is normal friendship")

    class Meta:
        db_table = 'wx_friendship'

class WxJobApplicant(models.Model):
    id = BigAutoField(primary_key=True)
    job_post = models.ForeignKey('WxJobPost', null=True, related_name='applicant_list')
    user = models.ForeignKey('WxUser', null=True)

    class Meta:
        db_table = 'wx_job_applicant'

class WxJobTitleSample(models.Model):
    id = BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'wx_job_title_sample'

class WxJobField(models.Model):
    id = BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'wx_job_field'

class WxJobPost(models.Model):
    # Job ID
    id = models.AutoField(primary_key=True)

    # Job posted user
    user = models.ForeignKey('WxUser', null=True)

    # Job title
    title = models.CharField(max_length=255)

    # Experienced years
    year_exp = models.IntegerField()

    # Job status
    status = models.IntegerField()

    # Job type - Full Time, Part Time, Contact, Freelancer values are available
    type = enum.EnumField(JobType, default = JobType.FULL_TIME)

    # Company logo url
    company_logo = models.CharField(max_length=1024)

    # Company name
    company_name = models.CharField(max_length=1024)

    # Company url
    company_url = models.CharField(max_length=1024)

    # Job post location for hiring 
    location = models.TextField()

    # Job level - Internship, Entry level, Mid level, Senior level values are available
    level = enum.EnumField(JobLevel, default = JobLevel.MID_LEVEL)

    # Job post published date
    publish_date = models.DateField(blank=True, null=True)

    # Salary range : min - max
    salary_min = models.IntegerField(default = 0)
    salary_max = models.IntegerField(default = 0)

    # Salary type : Yearly, Monthly, Hourly
    salary_type = enum.EnumField(SalaryType, default = SalaryType.YEARLY)

    # Salary currency : Euro, Dollar, Pound signs
    salary_currency = models.CharField(max_length=8, blank=True)

    # Job description
    description = models.TextField()

    # Job start date - date is for plan, not for calculating expire date
    start_date = models.DateField(blank=True, null=True)

    # Job expire days - calculated from published date
    day_expire = models.IntegerField(blank=True, null=True)

    # Notify when : Job notification condition
    notify_when = enum.EnumField(JobNotifyWhen, null = True)

    # Notify about : Job notification objective
    notify_about = enum.EnumField(JobNotifyAbout, null = True)

    # How many times did users view the job
    num_view = models.IntegerField(default = 0)
    
    class Meta:
        db_table = 'wx_job_post'

class WxJobPostEdu(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    job_post = models.ForeignKey(WxJobPost, blank=True, null=True, related_name='education_set')
    name = models.CharField(max_length=128, blank=True)

    class Meta:
        db_table = 'wx_job_post_edu'


class WxJobPostLang(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    job_post = models.ForeignKey(WxJobPost, blank=True, null=True, related_name= 'lang_set')
    name = models.CharField(max_length=128, blank=True)

    class Meta:
        db_table = 'wx_job_post_lang'


class WxJobPostQuality(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    job_post = models.ForeignKey(WxJobPost, blank=True, null=True, related_name='quality_set')
    name = models.CharField(max_length=128, blank=True)

    class Meta:
        db_table = 'wx_job_post_quality'

class WxJobPostSkill(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    job_post = models.ForeignKey(WxJobPost, blank=True, null=True, related_name='skill_set')
    name = models.CharField(max_length=1024)

    class Meta:
        db_table = 'wx_job_post_skill'


class WxHobby(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=128)

    class Meta:
        db_table = 'wx_hobby'


class WxJobDescTemplate(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=128)
    content = models.TextField()
    created_user = models.ForeignKey('WxUser', db_column='created_user', blank=True, null=True)

    class Meta:
        db_table = 'wx_job_desc_template'


class WxLang(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=49, blank=True)
    iso_code = models.CharField(max_length=2, blank=True)

    class Meta:
        db_table = 'wx_lang'


class WxLicenceType(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=1024, blank=True)

    class Meta:
        db_table = 'wx_licence_type'


class WxMarketType(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, blank=True)

    class Meta:
        db_table = 'wx_market_type'

class WxNotification(models.Model):
    id = models.AutoField(primary_key=True)
    type = enum.EnumField(NotificationType, default=NotificationType.PROFILE_PROGRESS)
    receipt_id = models.IntegerField()
    comment = models.CharField(max_length=1024)
    is_read = models.IntegerField()
    created_date = models.DateTimeField()
    detail = models.TextField(blank=True)

    class Meta:
        db_table = 'wx_notification'
        ordering = ['-created_date']

    @staticmethod
    def createNotification(typeID, receipt, comment, detail):
        notification = WxNotification()

        if type(receipt) is object:
            notification.receipt_id = receipt.id
        else:
            notification.receipt_id = receipt

        notification.detail = json.dumps(detail, default = decimal_default)

        notification.comment = comment
        notification.type = typeID
        notification.is_read = 0
        notification.created_date = datetime.datetime.now(pytz.UTC)
        return notification

class WxQuality(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=128, blank=True)
    group = models.ForeignKey('self', blank=True, null=True, related_name='child_quality')

    class Meta:
        db_table = 'wx_quality'

class WxReview(models.Model):
    id = models.IntegerField(primary_key=True)
    reviewer = models.IntegerField(blank=True, null=True)
    reviewed = models.IntegerField(blank=True, null=True)
    mark = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'wx_review'


class WxSkills(models.Model):
    skill_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    group = models.ForeignKey('self', blank=True, null=True, related_name='child_skill')

    class Meta:
        db_table = 'wx_skills'


class WxUserLang(models.Model):
    user_lang_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(WxUser, blank=True, null=True, related_name = 'lang')
    lang = models.ForeignKey(WxLang, blank=True, null=True, help_text = "Language pk here")

    class Meta:
        db_table = 'wx_user_lang'


class WxUserLicence(models.Model):
    user_licence_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(WxUser, blank=True, null=True, related_name = 'licence')
    licence_name = models.CharField(max_length=1024, blank=True)

    class Meta:
        db_table = 'wx_user_licence'


class WxUserSkill(models.Model):
    user_skill_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(WxUser, related_name = 'skill', null=True)
    skill_name = models.CharField(max_length=1024)
    skill_point = models.DecimalField(max_digits=10, decimal_places=7, default = 0)
    skill_priority = models.IntegerField(default = 0)

    class Meta:
        db_table = 'wx_user_skill'
        ordering = ('skill_priority', )


class WxUserTimeline(models.Model):
    timeline_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(WxUser, related_name = 'timeline', null=True)
    work_title = models.CharField(max_length=255)
    work_place = models.CharField(max_length=255)
    type = models.IntegerField()
    year_from = models.IntegerField()
    year_to = models.IntegerField()

    class Meta:
        db_table = 'wx_user_timeline'
        ordering = ('year_to', )

class WxUserDevice(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    user = models.ForeignKey(WxUser, blank=True, null=True, related_name = "devices")
    reg_token = models.CharField(max_length=4096, blank=True)
    device_type = enum.EnumField(DeviceType, default=DeviceType.IOS_DEVICE)

    class Meta:
        db_table = 'wx_user_device'

    @staticmethod
    def registerDeviceToken(user, device_token, device_type):
        if device_token != None:
            try:
                device = WxUserDevice.objects.get(reg_token=device_token)
                device.user = user
                device.device_type = device_type
                device.save()
            except ObjectDoesNotExist as ex:
                device = WxUserDevice()
                device.user = user
                device.reg_token = str(device_token)
                device.device_type = device_type
                device.save()
            return device
        else:
            return None

            
class CometchatChatroommessages(models.Model):
    userid = models.IntegerField()
    chatroomid = models.IntegerField()
    message = models.TextField()
    sent = models.IntegerField()
    read = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cometchat_chatroommessages'


class WxCustomer(models.Model):
    id = models.AutoField(primary_key = True)
    sale_user = models.ForeignKey(WxUser, null = True, related_name = 'customer_list')
    name = models.CharField(max_length=64, blank=True)
    company_name = models.CharField(max_length = 64, blank=True)
    skype = models.CharField(max_length = 32, blank=True)
    phone = models.CharField(max_length = 32, blank=True)
    mail = models.CharField(max_length = 32, blank=True)
    city = models.CharField(max_length = 32, blank=True)
    postcode = models.CharField(max_length = 32, blank=True)
    country = models.CharField(max_length = 32, blank=True)
    PRIORITY = Choices((0, 'L', 'L'), (1, 'M', 'M'), (2, 'H', 'H'))
    priority = models.IntegerField(choices = PRIORITY, default = PRIORITY.L, help_text="0-'L', 2-'H'")
    created_date = models.DateTimeField(auto_now_add = True)
    contact_date = models.DateTimeField(null = True, blank = True)
    addr1 = models.CharField(max_length = 64, blank = True)
    addr2 = models.CharField(max_length = 64, blank = True)
    class Meta:
        db_table = 'wx_customer'

class WxCustomerFormTemplate(models.Model):
    id = models.AutoField(primary_key = True)
    sale_user = models.ForeignKey(WxUser, null = True)
    name = models.CharField(max_length = 32, blank = True)
    data = models.TextField(blank = True)
    class Meta:
        db_table = 'wx_customer_form'

class WxDeal(models.Model):
    id = models.AutoField(primary_key=True)
    #name = models.CharField(max_length=256, blank=True)
    #companyName = models.CharField(max_length=256, blank=True)
    title = models.CharField(max_length=256, blank=True)
    value = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    # USD, GBP, RUB, CNY
    valueCurrency = models.CharField(max_length=20, blank=True, default = 'USD')
    enum.EnumField(NotificationType, default=NotificationType.PROFILE_PROGRESS)
    # Leads = 1000, Contacted, Offer made, In negotiation, Deal Closed
    stage = enum.EnumField(DealStageType, default = DealStageType.LEAD, 
        help_text = "Leads-1000, Contacted-1001, Offer made-1002, In negotiation-1003, Deal Closed-1004")
    closingExpDate = models.DateTimeField(blank=True, null=True)
    closingProbability = models.IntegerField(default = 0)
    note = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(WxUser, related_name = 'deals')
    customer = models.ForeignKey(WxCustomer, related_name = 'deals', null = True)

    class Meta:
        db_table = 'wx_deals'       

class WxDealInvoice(models.Model):
    id = models.AutoField(primary_key=True)
    appearedDate = models.DateTimeField(blank=True, null=True)
    # invoiceDate = models.DateTimeField(blank=True, null=True)
    paymentDue = models.DateTimeField(blank=True, null=True)
    # customerName = models.CharField(max_length=256, blank=True)
    # customerTitle = models.CharField(max_length=256, blank=True)
    # customerCompanyName = models.CharField(max_length=256, blank=True)
    # customerCompanyAddress = models.CharField(max_length=256, blank=True)
    # companyName = models.CharField(max_length=256, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deal = models.OneToOneField(WxDeal, related_name = 'invoice')


    class Meta:
        db_table = 'wx_deal_invoices'

class WxDealInvoiceProduct(models.Model):
    id = models.AutoField(primary_key=True)
    product = models.CharField(max_length=256, blank=True)
    q = models.IntegerField(default=0)
    netPrice = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    tax = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    invoice = models.ForeignKey(WxDealInvoice, related_name = 'products')

    class Meta:
        db_table = 'wx_deal_invoice_products'

class WxDealHistory(models.Model):
    id = models.AutoField(primary_key=True)
    value = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    stage = enum.EnumField(DealStageType, default = DealStageType.LEAD)
    created_at = models.DateTimeField(auto_now_add=True)
    deal = models.ForeignKey(WxDeal, related_name = 'histories')

    class Meta:
        db_table = 'wx_deal_histories'


# class WxTeam(models.Model):
#     id = models.CharField(max_length = 32, primary_key = True)
#     name = models.CharField(max_length = 256, blank = True)

class WxTeamMember(models.Model):
    id = models.AutoField(primary_key = True)
    position = models.CharField(max_length=256, blank = True)
    tier = models.IntegerField(default = 1)
    team_uid = models.CharField(max_length = 64)
    user = models.ForeignKey(WxUser, related_name = 'team_member')
    inviter = models.ForeignKey(WxUser, null = True)        # User who invited this team member
    invite_uid = models.CharField(max_length = 64)          # Unique id of invitation.
    status = enum.EnumField(TeamMemberStatusType, default = TeamMemberStatusType.INVITED) # Team member status, 0:invited, 1: active

    class Meta:
        db_table = 'wx_team_member'