"""
Django settings for grid project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from pathlib import Path
import djcelery

BROKER_URL = 'amqp://guest:guest@localhost:5672/'

BASE_DIR = os.path.abspath(str(Path(__file__).parents[2]))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'l1^r&p5pj5$!gtke!wl3f&%z2ur6h_y2eusq(!ml5-6k+7ux=4'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_swagger',
    'grid',
    'service',
    'djcelery',
)

EMAIL_HOST='smtp.mailgun.org'
EMAIL_PORT=587
EMAIL_USERNAME='postmaster@mail.grid.systems'
EMAIL_PASSWORD='7ca4d5732db623319ca058df4c42df7f'
DEFAULT_FROM_EMAIL='no-reply@mail.grid.systems'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'grid.urls'

WSGI_APPLICATION = 'grid.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.mysql',
		'USER' : 'root',
		'PASSWORD' : '',
		'HOST' : 'localhost'
	}
}

TEMPLATE_DIRS = (
	BASE_DIR + '/templates/',
)
# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT='static'
REST_FRAMEWORK = {
	'DEFAULT_AUTHENTICATION_CLASSES': (
		# 'rest_framework.authentication.SessionAuthentication',
		'service.authentication.ExpiringTokenAuthentication',
		#'rest_framework.authentication.TokenAuthentication',
	)
}

AUTH_USER_MODEL = 'grid.WxUser'
SWAGGER_SETTINGS = {
	'exclude_namespaces': [],
	'api_version': '0.1',
	'api_path': '/',
	'enabled_methods': [
		'get',
		'post',
		'put',
		'delete'
	],
	'api_key': '',
	'is_authenticated': False,
	'is_superuser': False,
	'permission_denied_handler': None,
	'info': {
		'contact': 'songxunzhao1991@gmail.com',
		'description': 'This documentation lists Restful APIs for Grid Waxara App. Created by Z.S.X',
		'title': 'Grid APIs',
	},
	'doc_expansion': 'none',
}

UPLOAD_PATH = BASE_DIR + "/assets/"
MEDIA_URL = "http://s18220304.onlinehome-server.info/static/" # This url should change after uploading.

CHAT_SERVICE_URL = "http://s18220304.onlinehome-server.info/cometchat/rpc_apis/cometchat_api.php"
