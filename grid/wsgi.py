"""
WSGI config for grid project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os
import djcelery

djcelery.setup_loader()

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "grid.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
